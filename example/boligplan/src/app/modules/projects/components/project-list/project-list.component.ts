import { Component, Input, OnInit } from '@angular/core';
import { Campaign } from '../../../../core/models/campaign';
import { Globals } from '../../../../Globals';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {

  @Input() campaigns: Campaign[] = [];
  @Input() adminView = false;
  public globalConfig;

  constructor(private globals: Globals) {}

  ngOnInit() {
    this.globalConfig = this.globals.config;
  }

  public view(uuid: string): void {

  }

}

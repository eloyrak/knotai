import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CrudService } from '../../../../core/http/crud.service';
import { UserService } from '../../../../core/http/user.service';
import { Globals } from '../../../../Globals';
import { FormErrorHandlerService } from '../../../../core/services/form-error-handler.service';
import { Location } from '@angular/common';
import { Campaign } from '../../../../core/models/campaign';
import { Status } from '../../../../core/models/status';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { Municipality } from '../../../../core/models/municipality';

@Component({
  selector: 'app-dummy-project-add',
  templateUrl: './dummy-project-add.component.html',
  styleUrls: ['./dummy-project-add.component.scss']
})
export class DummyProjectAddComponent implements OnInit {

  addProjectForm = this.fb.group({
    att_title: ['', Validators.required],
    att_selected: [0, [Validators.required, Validators.min(0)]],
    att_visited: [0, [Validators.required, Validators.min(0)]],
    att_status: [null, Validators.required],
    att_municipality: [null, Validators.required],
  });

  public statuses: Status[];
  public municipalities: Municipality[];

  constructor(private crud: CrudService,
              private users: UserService,
              private globals: Globals,
              private errorHandler: FormErrorHandlerService,
              private location: Location,
              private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.crud.getCollection(Status).subscribe((statusModels: JsonApiQueryData<Status>) => {
      this.statuses = statusModels.getModels();
    });
    this.crud.getCollection(Municipality).subscribe((municipalityModels: JsonApiQueryData<Municipality>) => {
      this.municipalities = municipalityModels.getModels();
    });
  }

  public onSubmit(): void {
    const projectData = {
      title: this.addProjectForm.getRawValue()['att_title'],
      selected: this.addProjectForm.getRawValue()['att_selected'],
      visited: this.addProjectForm.getRawValue()['att_visited'],
      status: this.addProjectForm.getRawValue()['att_status'],
      municipality: this.addProjectForm.getRawValue()['att_municipality']
    };
    console.log(projectData);
    this.crud.createAndSave(Campaign, projectData)
      .toPromise()
      .then((project: Campaign) => {
        console.log(project);
      })
      .catch(() => console.error(`baia baia`));
  }

}

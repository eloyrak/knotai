import { Component, OnInit } from '@angular/core';
import { Campaign } from '../../../../core/models/campaign';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { CrudService } from '../../../../core/http/crud.service';
import { UserService } from '../../../../core/http/user.service';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss']
})
export class ArchiveComponent implements OnInit {

  campaigns: Campaign[];
  isAdmin: boolean;

  constructor(
    private crud: CrudService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.getCurrentUser()
      .then(user => {
        this.isAdmin = this.userService.isAdmin(user);
        this.loadProjects();
      });
  }

  private loadProjects(): void {
    this.crud.getCollection(Campaign, ['status', 'municipality']).subscribe((projectModels: JsonApiQueryData<Campaign>) => {
      this.campaigns = projectModels.getModels();
    });
  }

}

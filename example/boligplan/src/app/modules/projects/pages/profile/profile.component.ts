import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChangePasswordComponent } from '../../modals/change-password/change-password.component';
import { ChangeEmailComponent } from '../../modals/change-email/change-email.component';
import { User } from '../../../../core/models/user';
import { UserService } from '../../../../core/http/user.service';
import { EditableFieldComponent } from '../../../../shared/components/editable-field/editable-field.component';
import { CrudService } from '../../../../core/http/crud.service';
import { FormErrorHandlerService } from '../../../../core/services/form-error-handler.service';
import { Campaign } from '../../../../core/models/campaign';
import { JsonApiQueryData } from 'angular2-jsonapi';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @ViewChild('firstName') firstNameInput: EditableFieldComponent;
  @ViewChild('lastName') lastNameInput: EditableFieldComponent;
  public currentUser: User;
  public isAdmin: boolean;
  public email = '';
  public municipalityName: string = null;
  public errors = {};

  public campaigns: Campaign[];

  constructor(
    private modalService: NgbModal,
    private userService: UserService,
    private crud: CrudService,
    private errorHandler: FormErrorHandlerService
  ) {}

  ngOnInit() {
    this.userService.getCurrentUser(true)
      .then(user => {
        this.currentUser = user;
        this.isAdmin = this.userService.isAdmin(user);
        this.loadUserData();
        this.loadProjects();
      })
      .catch(() => this.currentUser = null);
  }

  private loadUserData(): void {
    this.email = this.currentUser.mail ? this.currentUser.mail : 'Ukendt';
    this.municipalityName = this.currentUser.municipality ? this.currentUser.municipality.name : this.userService.isAdmin(this.currentUser) ? null : 'Ikke tildelt';
  }

  private loadProjects(): void {
    this.crud.getCollection(Campaign, ['status', 'municipality']).subscribe((projectModels: JsonApiQueryData<Campaign>) => {
      this.campaigns = projectModels.getModels();
    });
  }

  public changeEmail() {
    const activeModal = this.modalService.open(ChangeEmailComponent, {size: 'sm', centered: true});
    activeModal.componentInstance.user = this.currentUser;
    activeModal.result.then((res: string) => {
      if (res === 'success') {
        this.userService.getCurrentUser(true)
          .then((user: User) => {
            this.currentUser = user;
            this.loadUserData();
          })
          .catch(() => this.currentUser = null);
      }
      else {
        console.warn(`Modal window closed for unknown reason.`);
      }
    }).catch(err => {
      // When 'err' is 0 it means the modal has been closed clicking outside of it, and we ignore it.
      if (err !== 0) { console.error(err); }
    });
  }

  public changePassword() {
    const activeModal = this.modalService.open(ChangePasswordComponent, {centered: true});
    activeModal.componentInstance.user = this.currentUser;
    activeModal.result.then((res: string) => {
      if (res === 'success') {
        this.userService.getCurrentUser(true)
          .then((user: User) => {
            this.currentUser = user;
            this.loadUserData();
          })
          .catch(() => this.currentUser = null);
      }
      else {
        console.warn(`Modal window closed for unknown reason.`);
      }
    }).catch(err => {
      // When 'err' is 0 it means the modal has been closed clicking outside of it, and we ignore it.
      if (err !== 0) { console.error(err); }
    });
  }

  changeFirstName(newFirstName: string): void {
    this.errors = {};
    this.firstNameInput.showSubmitting();
    this.userService.boligplanUserEdit(this.currentUser.internalId, { 'param': 'first_name', 'new_value': newFirstName })
      .then(() => {
        this.currentUser.firstName = newFirstName;
        this.firstNameInput.hide();
      })
      .catch(err => {
        console.error(err);
        this.errors = this.errorHandler.processDrupalErrors(this.errors, err);
        console.error(this.errors);
        this.firstNameInput.hide();
      });
  }

  changeLastName(newLastName: string): void {
    this.errors = {};
    this.lastNameInput.showSubmitting();
    this.userService.boligplanUserEdit(this.currentUser.internalId, { 'param': 'last_name', 'new_value': newLastName })
      .then(() => {
        this.currentUser.lastName = newLastName;
        this.lastNameInput.hide();
      })
      .catch(err => {
        console.error(err);
        this.errors = this.errorHandler.processDrupalErrors(this.errors, err);
        console.error(this.errors);
        this.lastNameInput.hide();
      });
  }

}

import { Component, OnInit } from '@angular/core';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { User } from '../../../../core/models/user';
import { UserService } from '../../../../core/http/user.service';
import { CrudService } from '../../../../core/http/crud.service';
import { Globals } from '../../../../Globals';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from '../../../../shared/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})

export class UserListComponent implements OnInit {

  public users: User[] = [];
  private currentUser: User;
  public privileges = {};
  public show = false;
  public toDelete: User;

  constructor(
    private userserv: UserService,
    private crud: CrudService,
    private globals: Globals,
    private router: Router,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.load();
  }

  private load(): void {
    this.users = [];
    this.currentUser = null;
    this.privileges = {};
    // show = false;
    this.toDelete = null;

    this.userserv.getCurrentUser()
      .then((user: User) => {
        this.currentUser = user;
        this.analyseCurrentUser();
        this.getUsers()
          .then(() => {
            this.prepareUsers()
              .then(() => {
                this.show = true;
              })
              .catch((err) => {
                console.warn(`Couldn't prepare users`);
                console.error(err);
              });
          })
          .catch((err) => {
            console.warn(`Couldn't retrieve users.`);
            console.error(err);
          });
      })
      .catch((err) => {
        console.error(err);
      });
  }

  /**
   * Establishes the current user's administrative privileges.
   */
  private analyseCurrentUser(): void {
    for (const role of this.currentUser.roles) {
      this.privileges['admin'] = this.privileges['admin'] || (role.is_admin || this.globals.generalConfig.administratorRoles.includes(role.internalId));
      this.privileges['municipal'] = this.privileges['municipal'] || this.globals.generalConfig.municipalAdministratorRoles.includes(role.internalId);
    }
    this.privileges['add'] = this.privileges['admin'] || this.privileges['municipal'];
  }

  public getUsers(): Promise<void> {
    return new Promise((resolve, reject) => {
      // We filter the users based on the current user municipality and privileges.
      this.crud.getCollection(User, ['roles', 'municipality'], {}, this.crud.getComplexFilter(this.genUserFilter())).subscribe((userModels: JsonApiQueryData<User>) => {
        this.users = userModels.getModels();
      }, () => { reject(); }, () => { resolve(); });
    });
  }

  /**
   * Determines which users we need to show on the user list based on the current user privileges and municipality.
   * For a municipality user we show only himself. For a municipality admin we show other municipality admins and municipality users
   * for that same municipality. For an administrator we show everyone.
   */
  private genUserFilter(): object {
    // Check getComplexFilter docs.
    const filter = {
      g0: {
        condition: 'AND',
        queries: {
          internalId: {
            operator: '<>',
            values: [this.currentUser.internalId]
          }
        }
      }
    };
    if (!this.userserv.isAdmin(this.currentUser)) {
      filter['g0']['queries']['municipality.internalId'] = {
        operator: '=',
        values: [this.currentUser.municipality ? this.currentUser.municipality.internalId : '']
      };
    }
    return filter;
  }

  /**
   * Iterates over the users to set necessary information for the form (municipality and operations).
   */
  private prepareUsers(): Promise<void> {
    // (key: string => value: boolean). Maps (by user uuid) if current user can edit/delete each of the users in the list.
    this.privileges['edit'] = {};
    this.privileges['delete'] = {};
    const date = new Date();
    this.currentUser.municipality_placeholder = this.privileges['admin'] ? 'All' : this.currentUser.municipality ? this.currentUser.municipality.name : 'None';

    return new Promise((resolve) => {
      for (const user of this.users) {
        user.memberfor = Globals.formatTime(date.getTime() - user.createdAt.getTime());
        user.lastaccess = Globals.formatTime(date.getTime() - user.lastLogin.getTime()) + ' ago';
        user.municipality_placeholder = this.determineMunicipality(user);
        this.privileges['edit'][user.id] = this.determineEditPrivileges(user);
        // For now if you can edit, you can delete.
        this.privileges['delete'][user.id] = this.privileges['edit'][user.id];
      }
      resolve();
    });
  }

  /**
   * Identifies to which municipality belongs the user. Administrators belong to all of them, and there could be users without one.
   * For display purposes.
   */
  private determineMunicipality(user: User): string {
    if (user.municipality) { return user.municipality.name; }
    else if (this.userserv.isAdmin(user)) { return ''; }
    else { return 'Ikke tildelt'; }
  }

  /**
   * For each user retrieved for the list, determines whether the current user can edit/delete their information or not.
   * True when it can edit.
   * A user 'A' can edit/delete a user 'B' when:
   *  - A is an administrator
   *  - A is a municipal administrator and B belongs to that municipality AND B isn't himself an administrator or municipality administrator
   *  - A and B are the same person
   */
  private determineEditPrivileges(user: User): boolean {
    return (this.privileges['admin'] || user.id === this.currentUser.id) ||
      (this.privileges['municipal'] && this.currentUser.municipality.internalId === user.municipality.internalId &&
        !this.userserv.hasRole(user, this.globals.generalConfig.municipalAdministratorRoles.concat(this.globals.generalConfig.administratorRoles)));
  }

  public edit(user: User): void {
    this.router.navigate(['admin', 'users', 'edit', user.id]).catch(err => console.error(err));
  }

  public open(modal, user: User) {
    this.toDelete = user;
    const deleteModal = this.modalService.open(ConfirmModalComponent, {centered: true});
    deleteModal.componentInstance.title = 'Slet bruger';
    deleteModal.componentInstance.text = `Er du sikker på, at du vil slette brugeren <b>${user.mail}</b>?`;
    deleteModal.result.then((result) => {
      console.log(`Deleting user ${user.mail}`);
      this.delete(user)
        .then(() => {
          this.load();
        })
        .catch((err) => {
          console.error(err);
        });
    }, () => {
      // Here there is a "reason" argument available if needed.
      console.log(`The delete confirmation was dismissed. No action will be taken.`);
    });
  }

  public delete(user: User): Promise<object> {
    return this.crud.delete(User, user.id).toPromise();
  }
}

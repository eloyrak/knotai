import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../../core/authentication/authentication.service';
import { Router } from '@angular/router';
import { UserService } from '../../../../core/http/user.service';
import { Globals } from '../../../../Globals';
import { FormBuilder, Validators } from '@angular/forms';
import { Alert } from '../../../../shared/models/alert';
import { DrupalService } from '../../../../core/http/drupal.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });
  submitting = false;
  alerts: { [id: string]: Alert } = {};

  constructor(
    private auth: AuthenticationService,
    private users: UserService,
    private drupal: DrupalService,
    private router: Router,
    private globals: Globals,
    private fb: FormBuilder
  ) {}

  ngOnInit() {}

  /*login(): void {
    const loginData = this.loginForm.value;
    this.submitting = true;
    delete this.alerts['login-failed'];
    this.users.loginNoScope(loginData.username, loginData.password)
      .then(() => {
        this.submitting = false;
        if (this.drupal.shouldRedirect(this.globals.currentUser)) {
          this.drupal.access(loginData.username, loginData.password)
            .then(() => {
              // TODO: Poner bien la ruta.
              /!*this.router.navigate(['/externalRedirect', { externalUrl: this.globals.generalConfig.goToDrupalUrl }])
                .catch((err) => {
                  console.error(err);
                });*!/
              this.router.navigate([`/user/campaigns`]).catch(err => console.error(err));
            })
            .catch((err) => console.error(err));
        }
        else {
          this.router.navigate([`/user/campaigns`]).catch(err => console.error(err));
        }
      })
      // TODO: Dejar el mensaje de error de Drupal?
      .catch((err) => {
        this.submitting = false;
        this.alerts['login-failed'] = {
          type: 'danger',
          message: err,
          dismissible: true
        };
      });
  }*/
  login(): void {
    const loginData = this.loginForm.value;
    this.submitting = true;
    delete this.alerts['login-failed'];
    this.users.loginNoScope(loginData.username, loginData.password)
      .then(() => {
        this.drupal.access(loginData.username, loginData.password)
          .then(() => {
            if (this.drupal.shouldRedirect(this.globals.currentUser)) {
              // TODO: Poner bien la ruta.
              /*this.router.navigate(['/externalRedirect', { externalUrl: this.globals.generalConfig.goToDrupalUrl }])
                .catch((err) => {
                  console.error(err);
                });*/
              this.router.navigate([`/user/campaigns`]).catch(err => console.error(err));
            }
            else {
              this.router.navigate([`/user/campaigns`]).catch(err => console.error(err));
            }
          })
          .catch((err) => console.error(err));
      })
      // TODO: Dejar el mensaje de error de Drupal?
      .catch((err) => {
        this.alerts['login-failed'] = {
          type: 'danger',
          message: err,
          dismissible: true
        };
      })
      .then(() => {
        this.submitting = false;
      });
  }
}

import { transition, trigger, query as q, style, animate, group, sequence, stagger } from '@angular/animations';

const query = (selector, animation, options = {optional: true}) => q(selector, animation, options);

export const homeRouterTransition = trigger('homeRouterTransition', [
  transition('* <=> *', [
    query(':enter', style({
      width: 0,
      paddingLeft: 0,
      paddingRight: 0
    })),
    query(':enter h3, :enter form > *', style({
      opacity: 0
    })),
    sequence([
      group([
        query(':leave', animate('300ms ease-in', style({
          width: 0,
          paddingLeft: 0,
          paddingRight: 0
        }))),
        query(':leave h3, :leave form > *', stagger(-30, animate('70ms ease-in-out', style({
          opacity: 0
        }))))
      ]),
      group([
        query(':enter', animate('500ms ease-out', style({
          width: '*',
          paddingLeft: '*',
          paddingRight: '*'
        }))),
        query(':enter h3, :enter form > *', stagger(100, animate('250ms 400ms ease-in-out', style({
          opacity: 1
        }))))
      ]),
    ]),
  ])
]);

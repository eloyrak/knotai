import { Attribute, JsonApiModel, JsonApiModelConfig } from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'menus'
})
export class Menu extends JsonApiModel {

  @Attribute()
  internalId: string;

  @Attribute()
  label: string;

  @Attribute()
  description: string;

}

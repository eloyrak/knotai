import { Attribute, JsonApiModel, JsonApiModelConfig } from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'articles'
})
export class Article extends JsonApiModel {

  @Attribute()
  internalId: string;

  @Attribute()
  title: string;

}

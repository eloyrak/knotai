// TODO: Replace or remove.
export class ProjectMetadata {

  id: string;
  name: string;
  status: 'done' | 'draft';
  lastAction: { action: string, date: string, hour: string };
  stats: { desc: string, val: string | number }[];

}

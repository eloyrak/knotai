import { Injectable } from '@angular/core';
import { DatastoreConfig, JsonApiDatastore, JsonApiDatastoreConfig } from 'angular2-jsonapi';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { MenuLink } from '../models/menu-link';
import { Menu } from '../models/menu';
import { Article } from '../models/article';
import { User } from '../models/user';
import { Role } from '../models/role';
import { Municipality } from '../models/municipality';
import { File } from '../models/file';
import { Campaign } from '../models/campaign';
import { Status } from '../models/status';

const config: DatastoreConfig = {
  baseUrl: environment.jsonapi,
  models: {
    menuLinks: MenuLink,
    menus: Menu,
    articles: Article,
    users: User,
    roles: Role,
    municipalities: Municipality,
    files: File,
    campaigns: Campaign,
    status: Status,
  }
};

@Injectable()
@JsonApiDatastoreConfig(config)
export class Datastore extends JsonApiDatastore {

  constructor(http: HttpClient) {
    super(http);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Globals } from '../../Globals';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient,
              private globals: Globals,
              private auth: AuthenticationService
  ) { }

  public uploadFile(file: File, entity_type: string, bundle: string, field_name: string): Promise<object> {
    let token: string;
    return new Promise((resolve, reject) => {
      if (!(token = localStorage['csrf_token'])) {
        reject(`The upload couldn't start because the user isn't logged into Drupal.`);
      }
      else {
        this.http.post(
          `${this.globals.env.fileUpload}/${entity_type}/${bundle}/${field_name}?_format=json`,
          file,
          {
            headers: new HttpHeaders({
              // We need to include the Authorization header because in specifying a set of headers we overwrite the automatic OAuth
              // header creation.
              'Authorization': `Bearer ${this.auth.getToken()}`,
              'X-CSRF-Token': `${token}`,
              'Content-Disposition': `file; filename="${file.name}"`,
              'Content-Type': `application/octet-stream`
              })
          })
          .toPromise()
          .then((res) => {
            resolve(res);
          })
          .catch((err) => reject(err));
      }
    });
  }
}

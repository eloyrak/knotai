import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Datastore } from '../services/datastore.service';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { User } from '../models/user';
import { AuthenticationService } from '../authentication/authentication.service';
import { Globals } from '../../Globals';
import { CrudService } from './crud.service';
import { Subject } from 'rxjs';
import { DrupalService } from './drupal.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public getCurrentUserChange = new Subject<User>();

  constructor(private http: HttpClient, private datastore: Datastore,
              private auth: AuthenticationService, private globals: Globals,
              private crud: CrudService, private drupal: DrupalService) { }

  /**
   * Makes a register request without password.
   *
   * Drupal will generate an access link and send it to the provided email at the moment of activation.
   *
   * @param username
   *   Username.
   * @param email
   *   Email.
   * @param timezone
   *   Timezone. Defaults to a generalConfig parameter.
   */
  public registerWithoutPass(username: string, email: string, timezone: string = this.globals.generalConfig.defaultTimezone): Promise<object> {
    return this.http.post(this.globals.env.register, {
      'name': {
        'value': username,
      },
      'mail': {
        'value': email,
      },
      'timezone': {
        'value': timezone,
      },
    })
      .toPromise();

  }

  /**
   * Makes a register request with password.
   *
   * @param username
   *   Username.
   * @param pass
   *   Password.
   * @param timezone
   *   Timezone. Defaults to generalConfig parameter.
   */
  public registerWithPass(username: string, pass: string, timezone: string = this.globals.generalConfig.defaultTimezone): Promise<object> {
    return this.http.post(this.globals.env.register, {
      'name': {
        'value': username,
      },
      'pass': {
        'value': pass,
      },
      'timezone': {
        'value': timezone,
      },
    })
      .toPromise();
  }

  /**
   * Sets the currentUser global variable.
   */
  public setCurrentUser(): Promise<object> {
    return new Promise((resolve, cancel) => {
      // isAuthenticated checks for valid token and tries to refresh it if it has expired.
      // Necessary to ensure that we have a valid token when retrieving the currentUser data (with getCurrentUser; see below) and not an expired one.
      this.auth.isAuthenticated()
        .then(() => {
          if (localStorage.getItem('current_user')) {
            this.crud.getIndividual(User, localStorage.getItem('current_user'), ['roles', 'municipality', 'municipality.image'])
              .toPromise()
              .then((user: User) => {
                this.globals.currentUser = user;
                this.getCurrentUserChange.next(this.globals.currentUser);
                resolve();
              }).catch((err) => cancel(err));
          }
          else {
            this.getUserIdFromToken(this.auth.getToken())
              .then((tokenInfo) => {
                this.crud.getCollection(User, ['roles', 'municipality', 'municipality.image'], {}, {
                  internalId: tokenInfo['id'],
                })
                  .subscribe((user: JsonApiQueryData<User>) => {
                    this.globals.currentUser = user.getModels()[0];
                    localStorage.setItem('current_user', this.globals.currentUser.id);
                    this.getCurrentUserChange.next(this.globals.currentUser);
                    resolve();
                  });
              }).catch((err) => cancel(err));
          }
        }).catch((err) => cancel(err));
    });
  }

  /**
   * Returns a Promise with a User argument onsuccess,
   * corresponding to the currently logged in user.
   *
   * Though currentUser is a global variable it should be
   * retrieved with this method to ensure it exists
   * whenever possible.
   *
   * This method checks if currentUser is already defined.
   * If it is, then it is returned.
   * If it isn't for whatever reason (i.e: the page was reloaded),
   * we try to refresh the authentication token and setting it.
   *
   * If it still returns null it most likely means the user is not
   * authenticated and should be logged out of the app.
   *
   * When forceReload is true we ignore if currentUser is defined and request it anyway.
   */
  public getCurrentUser(forceReload: boolean = false): Promise<User> {
    return new Promise((resolve, reject) => {
      if (forceReload || !this.globals.currentUser || this.globals.currentUser === null) {
        this.setCurrentUser()
          .then(() => resolve(this.globals.currentUser))
          .catch((err) => {
            console.warn(`Failed to get current user.`);
            reject(err);
          });
      } else {
        resolve(this.globals.currentUser);
      }
    });
  }

  /**
   * Tries to log in a user with a set of privileges.
   * The login process implies authenticating the user throught the authentication service and setting the global variable currentUser.
   */
  public login(username: string, password: string, roles: Array<string>): Promise<object> {
    return new Promise((resolve, reject) => {
      this.auth.authenticate(username, password, roles)
        .then(() => {
          this.setCurrentUser()
            .then((res) => resolve(res))
            .catch(() => reject('Unexpected error when logging in.'));
        })
        .catch(() => reject('The username and password are not valid.'));
    });
  }

  /**
   * Tries to log in a user with every privilege available to him.
   * The login process implies authenticating the user throught the authentication service and setting the global variable currentUser.
   */
  public loginNoScope(username: string, password: string): Promise<object> {
    return new Promise((resolve, reject) => {
      this.auth.authenticate(username, password)
        .then(() => {
          this.setCurrentUser()
            .then((res) => resolve(res))
            .catch(() => reject('Unexpected error when logging in.'));
        })
        .catch(() => reject('The username and password are not valid.'));
    });
  }

  /**
   * Logs out the current user. Unsets the currentUser global variable and checks if the user needs to alse be logged out from Drupal.
   */
  public logout(): Promise<object> {
    return new Promise((resolve, reject) => {
      this.getCurrentUser()
        .then(() => {
          if (this.auth.logout()) {
            this.globals.currentUser = null;
              this.drupal.isLoggedIn()
                .then((status) => {
                  if (status) {
                    this.drupal.logout()
                      .then(() =>  resolve())
                      .catch((err) => {
                        console.warn(`Failed to log out from Drupal.`);
                        reject(err);
                      });
                  }
                  else { resolve(); }
                })
                .catch((err) => reject(err));
          } else { reject(`Failed to log out from angular.`); }
        })
        .catch((err) => reject(err));
    });
  }

  /**
   * Logs out and then logs in. Necessary after certain actions to confirm that the user is legitimate.
   */
  public relogUser(username: string, password: string, roles: Array<string> = []): Promise<object> {
    return new Promise((resolve, reject) => {
      this.logout()
        .then(() => {
          if (roles && roles.length !== 0) {
            this.login(username, password, roles)
              .then(() => resolve())
              .catch((err) => reject(err));
          }
          else {
            this.loginNoScope(username, password)
              .then(() => resolve())
              .catch((err) => reject(err));
          }
        })
        .catch((err) => reject(err));
    });
  }

  /**
   * Gets the internalId of a User based on the token OAuth has generated for him.
   *
   * @param token
   *   OAuth2 token.
   */
  private getUserIdFromToken(token: string): Promise<object> {
    return this.http.get(this.globals.auth.token_info, {
      headers: new HttpHeaders(`Authorization: Bearer ${token}`)
    })
      .toPromise();
  }

  /**
   * Determines if a user has admin privileges.
   */
  public isAdmin(user: User): boolean {
    if (!user || !user.roles) { return false; }
    for (const role of user.roles) {
      if ((role.is_admin != null && role.is_admin) || this.globals.generalConfig.administratorRoles.includes(role.internalId)) { return true; }
    }
    return false;
  }

  /**
   * Determines if a user has ANY role in an array of roles' internalIds.
   */
  public hasRole(user: User, roleInternalId: string[]): boolean {
    if (!user || typeof user.roles === 'undefined') { return false; }
    for (const role of user.roles) { if (roleInternalId.includes(role.internalId)) { return true; } }
    return false;
  }

  /**
   * Returns an array of internalIds of the roles a certain user can grant to other users when creating or editing users.
   */
  public rolesToGrant(user: User): Array<string> {
    const roles = [];
    for (const role of user.roles) {
      if (this.globals.generalConfig.canGrant[role.internalId]) {
        for (const internalRoleName of this.globals.generalConfig.canGrant[role.internalId]) {
          if (!roles.includes(internalRoleName)) { roles.push(internalRoleName); }
        }
      }
    }
    return roles;
  }

  public boligplanUserEdit(userToEditInternalId: number, data: { [key: string]: string }): Promise<object> {
    return new Promise((resolve, reject) => {
      if (!localStorage.getItem('access_token') || !localStorage.getItem('csrf_token')) {
        reject(`Access token and CSRF token needed to perform this action.`);
      }
      else {
        const headers: HttpHeaders = new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.auth.getToken()}`,
          'X-CSRF-Token': localStorage.getItem('csrf_token')
        });

        this.http.post(`${this.globals.env.boligplanUserEdit}/${userToEditInternalId}?_format=json`, data, { headers: headers })
          .toPromise()
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      }
    });
  }
}

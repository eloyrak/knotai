import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Datastore } from '../services/datastore.service';
import { User } from '../models/user';
import { AuthenticationService } from '../authentication/authentication.service';
import { Globals } from '../../Globals';

@Injectable({
  providedIn: 'root'
})
export class DrupalService {

  constructor(private http: HttpClient, private datastore: Datastore,
              private auth: AuthenticationService, private globals: Globals
  ) { }


  /**
   * Checks if any user is logged into Drupal.
   */
  public isLoggedIn(): Promise<boolean> {
    return this.http.get(`${this.globals.env.logStatus}?_format=json`, { withCredentials: true })
      .toPromise()
      .then((res) => JSON.stringify(res) === '1')
      .catch((err) => err);
  }

  /**
   * Logs a user into the Drupal backend and sets the csrf and logout tokens in local storage.
   */
  public login(username: string, password: string): Promise<object> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.globals.env.login}?_format=json`, {
        'name': username,
        'pass': password,
      }, {withCredentials: true})
        .toPromise()
        .then((res) => {
          localStorage.setItem('csrf_token', res['csrf_token']);
          localStorage.setItem('logout_token', res['logout_token']);
          resolve(res);
        })
        .catch((err) => {
          console.warn(`Failed to log in to Drupal.`);
          reject(err);
        });
    });
  }

  /**
   * Logs out from Drupal and clears the logout and csrf token.
   *
   * @returns
   *   Returns a successful promise if the logout works or a failed promise otherwise.
   */
  public logout(): Promise<object> {
    let logoutRoute = this.globals.env.logout;
    let logoutToken = null;
    if ((logoutToken = localStorage.getItem('logout_token')) != null) { logoutRoute += `?token=${logoutToken}&_format=json`; }
    return new Promise((resolve, reject) => {
      this.http.post(logoutRoute, {},
        {withCredentials: true})
        .toPromise()
        .then((res) => {
          if (logoutToken != null) {
            localStorage.removeItem('logout_token');
            localStorage.removeItem('csrf_token');
          }
          resolve(res);
        })
        .catch((err) => reject(err));
    });
  }

  /**
   * Checks if a user should be redirected to Drupal.
   */
  public shouldRedirect(user: User): boolean {
    if (user.roles === undefined) { return false; }
    for (const role of user.roles) {
      if (this.globals.generalConfig.goToDrupalRoles.includes(role.internalId)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Handles Drupal login and takes care of previous logins.
   */
  public access(username: string, password: string): Promise<object> {
    return this.isLoggedIn()
      .then((state) => {
        if (state) {
          this.logout()
            .then(() => {
              this.login(username, password)
                .then((res) =>  res)
                .catch((err) => err);
            })
            .catch(() => {
              this.login(username, password)
                .then((res) =>  res)
                .catch((err) => err);
            });
        }
        else {
          this.login(username, password)
            .then((res) => res)
            .catch((err) => err);
        }
      })
      .catch((err) => err);
  }
}

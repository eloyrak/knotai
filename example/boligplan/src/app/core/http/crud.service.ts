import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Datastore } from '../services/datastore.service';
import { Observable } from 'rxjs';
import { JsonApiModel, JsonApiQueryData, ModelType } from 'angular2-jsonapi';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient, private datastore: Datastore) { }

  /*********************
   ****** CREATE
   *********************/

  /**
   * Creates an entity. Not persisted on DB.
   *
   * @param model
   *   Type of model we want to create (User, Role, File...).
   * @param data
   *   A JSON object with appropriatly formated attributes.
   */
  public create<T extends JsonApiModel>(model: ModelType<T>, data: {}): T {
    return this.datastore.createRecord(model, data);
  }


  /*********************
   ******** READ
   *********************/

  /**
   * Returns an observable with a collection of entities.
   *
   * @param model
   *   ModelType we want to get (User, Role, File...).
   * @param relationships
   *   Relationships to include in the query.
   * @param pagination
   *   Optional parameter that specifies pagination.
   * @param filter
   *   Optional parameter that filters the returned collection.
   * @param sort
   *   Optional parameter for sorting result.
   *
   * If you want to set filters but not pagination, just send an empty object -> {}.
   */
  public getCollection<T extends JsonApiModel>
  (model: ModelType<T>, relationships: Array<string> = [], pagination: Object = {}, filter: Object = {}, sort?: string): Observable<JsonApiQueryData<T>> {
    return sort ?
      this.datastore.findAll(model, {
      page: pagination,
      filter: filter,
      sort: sort,
      include: relationships.join(','),
    }) :
    this.datastore.findAll(model, {
      page: pagination,
      filter: filter,
      include: relationships.join(','),
    });
  }

  /**
   * Returns a single entity.
   *
   * @param model
   *   ModelType we want to get (User, Role, File...).
   * @param uuid
   *   UUID of the user to return.
   * @param relationships
   *   Relationships to include in the query.
   */
  public getIndividual<T extends JsonApiModel>(model: ModelType<T>, uuid: string, relationships: Array<string> = []): Observable<T> {
    return this.datastore.findRecord(model, uuid, {
      include: relationships.join(','),
    });
  }

  /*********************
   ******* UPDATE
   *********************/

  /**
   * Persists entity to DB.
   *
   * Call this after modifying its attributes (directly
   * on the JsonApiModel object) so the changes are saved.
   *
   * @param model
   *   JsonApiModel to update on DB.
   * @param relationships
   *   Relationships you want to include in the saving process.
   * @param headers
   *   Optional heders to add to the request.
   */
  public update<T extends JsonApiModel>(model: T, relationships: Array<string> = [], headers?: Headers): Observable<T> {
    return model.save({
      include: relationships.join(',')
    },
      headers);
  }

  /**
   * Creates a User model and then saves it to DB. Not a register action.
   *
   * @param model
   *   Type of model we want to create and save (User, Role, File...).
   * @param data
   *   A JSON object with appropriatly formated user attributes.
   */
  public createAndSave<T extends JsonApiModel>(model: ModelType<T>, data: {}): Observable<T> {
    return this.update(this.create(model, data));
  }


  /*********************
   ******* DELETE
   *********************/

  /**
   * Deletes a record from DB.
   *
   * @param model
   *   Type of model we want to delete (User, Role, File...).
   * @param uuid
   *   UUID of the entity to delete.
   */
  public delete<T extends JsonApiModel>(model: ModelType<T>, uuid: string): Observable<Response> {
    return this.datastore.deleteRecord(model, uuid);
  }

  /**
   * Generates a properly formated filter object for use with this CRUD implementation.
   * @example The object it receives as argument must be of this format:
   * const rolesWeWant = {                               <- Filter object
      g0: {                                              <- Condition group. All conditions inside will be joined with the same logic operator
        condition: 'OR',                                 <- Logic operator to apply to al the conditions
        queries: {                                       <- Object containing the conditions
          internalId: {                                  <- Model field to match
            operator: '=',                               <- Match operator
            values: this.globals.config.manageableRoles  <- List of values to match
          }
        }
      }
    };
   * That object means: search entities whose internalId equals ('=') ANY ('OR') of the elements in the 'values' array.
   * This method transforms that object into a valid filter.
   *
   * The example object generates the following filter object:
   * filter = {
      "g0": {
        "group": {
          "conjunction": "OR"
        }
      },
      "f0": {
        "condition": {
          "path": "internalId",
          "operator": "=",
          "value": "administrator",
          "memberOf": "g0"
        }
      },
      "f1": {
        "condition": {
          "path": "internalId",
          "operator": "=",
          "value": "municipality_admin",
          "memberOf": "g0"
        }
      },
      "f2": {
        "condition": {
          "path": "internalId",
          "operator": "=",
          "value": "municipality_user",
          "memberOf": "g0"
        }
      }
    }
   *
   */
  public getComplexFilter(conditions: object): object {
    const filter = {};

    for (const gkey of Object.keys(conditions)) {
      filter[gkey] = {};
      filter[gkey]['group'] = {};
      filter[gkey]['group']['conjunction'] = conditions[gkey]['condition'];
      let i = 0;
      for (const key of Object.keys(conditions[gkey]['queries'])) {
        for (const value of conditions[gkey]['queries'][key]['values']) {
          filter['f' + i] = {};
          filter['f' + i]['condition'] = {};
          filter['f' + i]['condition']['path'] = key;
          filter['f' + i]['condition']['operator'] = conditions[gkey]['queries'][key]['operator'];
          filter['f' + i]['condition']['value'] = value;
          filter['f' + i]['condition']['memberOf'] = gkey;
          i++;
        }
      }
    }
    return filter;
  }
}

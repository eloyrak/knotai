import { Directive, ElementRef, Input, OnChanges, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBtnProgress]'
})
export class BtnProgressDirective implements OnInit, OnChanges {

  @Input() appBtnProgress = 0;

  constructor(
    private renderer: Renderer2,
    private el: ElementRef
  ) {}

  ngOnInit() {
    this.renderer.addClass(this.el.nativeElement, 'btn-loading');
  }

  ngOnChanges() {
    this.renderer.setStyle(this.el.nativeElement, 'background-size', this.appBtnProgress + '%');
  }

}

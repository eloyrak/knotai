import { AfterContentInit, Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appAutofocus]'
})
export class AutofocusDirective implements AfterContentInit {

  constructor(private elementRef: ElementRef) {}

  ngAfterContentInit() {
    this.elementRef.nativeElement.focus();
  }

}

import { Component, Input } from '@angular/core';
import { Alert } from '../../models/alert';
import { showHideAlert } from './show-hide-alert.animation';

@Component({
  selector: 'app-alert-group',
  templateUrl: './alert-group.component.html',
  styleUrls: ['./alert-group.component.scss'],
  animations: [showHideAlert]
})
export class AlertGroupComponent {

  @Input() alerts: Alert[] = [];

  constructor() {}

  get _alerts(): Alert[] {
    return Array.isArray(this.alerts) ? this.alerts : Object.keys(this.alerts).map(key => this.alerts[key]);
  }

  close(alert: Alert) {
    if (Array.isArray(this.alerts)) {
      this.alerts.splice(this.alerts.indexOf(alert), 1);
    } else {
      const key = Object.keys(this.alerts)[this._alerts.indexOf(alert)];
      delete this.alerts[key];
    }
  }

}

import { Component, Input, OnChanges } from '@angular/core';
import { showHideFeedback } from './show-hide-feedback.animation';

@Component({
  selector: 'app-input-feedback',
  templateUrl: './input-feedback.component.html',
  styleUrls: ['./input-feedback.component.scss'],
  animations: [showHideFeedback]
})
export class InputFeedbackComponent implements OnChanges {

  @Input() type = 'danger';
  @Input() messages: { [fieldname: string]: string[] } = {};
  _fields: string[] = [];
  _messages: string[] = [];

  @Input() set fields(fields: string) {
    this._fields = fields.length ? fields.split(',') : [];
  }

  constructor() {}

  ngOnChanges(): void {
    console.log('cambia');
    this._messages = [];
    for (const field of this._fields) {
      this._messages = this._messages.concat(this.messages[field]);
    }
  }

}

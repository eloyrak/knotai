## Iconos

En **negrita** os obrigatorios.
En _cursiva_ os recomendados.

#### Chrome
* `browser_action.default_icon`
    * 16
    * 24
    * 32
* `icons`
    * 16
    * 32 (Windows)
    * **48** (chrome://extensions)
    * **128** (Instalación en Chrome Web Store)
    
#### Edge
* `browser_action.default_icon`
    * 16
    * _20_
    * 32
    * _40_
* `icons`
    * **48**
    * **128**
    * **176**
* Empaquetado
    * **44** (Windows UI)
    * **50**
    * **150** (Microsoft Store)
    


## Empaquetado

#### Chrome

Crear un ficheiro comprimido cos contidos da carpeta, con extensión `.zip`

#### Edge

1. Instalar `manifoldjs` 

        npm install -g manifoldjs
        
2. Crear estrutura propia da extensión de Edge

        manifoldjs -l debug -p edgeextension -f edgeextension -m <RUTA_EXTENSION>\manifest.json
        
3. Modificar o ficheiro `appxmanifest.xml` en `<NOME_EXTENSION>\edgeextension\manifest` para actualizar o _Identity_ e o _PublisherDisplayName_ unha vez obtida a aprobación na tenda.

4. Empaquetar a extensión

        manifoldjs -l debug -p edgeextension package <NOME_EXTENSION>\edgeextension\manifest\

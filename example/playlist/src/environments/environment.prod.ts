export const environment = {
  production: true,
  availableLangs: ['en', 'es'],
  defaultLang: 'es',
  apiUrl: 'http://192.168.99.100:5000/api',
  deployUrl: '',
  imagesPath: 'assets/images/',
  iconClass: 'icon-netex',
  userId: '243bec60-1951-4c6a-8a06-53ee6a9f3e40'
};

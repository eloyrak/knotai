// This files can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of files replacements can be found in `angular.json`.

export const environment = {
  production: false,
  availableLangs: ['en', 'es'],
  defaultLang: 'en',
  apiUrl: 'http://172.28.128.2/PLAYLIST/public/api',
  deployUrl: '',
  iconClass: 'icon-netex',
  user: {
    id: '243bec60-1951-4c6a-8a06-53ee6a9f3e40',
    firstname: 'Juan',
    lastname: 'Rodríguez',
    email: 'juan@gmail.com'
  },
  imagesPath:  'assets/images/'
};

/*
 * For easier debugging in development mode, you can import the following files
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

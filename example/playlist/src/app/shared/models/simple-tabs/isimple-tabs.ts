import { TranslatableString } from '../translatable-string/translatable-string';

export interface ISimpleTabs {
  path: string;
  label: TranslatableString;
  disabled?: boolean;
}

import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { TranslatableString } from '../../models/translatable-string/translatable-string';

export declare type ToastType = 'success' | 'notification' | 'warn';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) {}

  public open(
    text: TranslatableString,
    type: ToastType = 'success',
    action?: TranslatableString,
    config?: MatSnackBarConfig
  ): MatSnackBarRef<SimpleSnackBar> {
    return this.snackBar.open(
      text.translate(this.translate),
      action && action.translate(this.translate),
      {panelClass: type, ...config}
    );
  }

  public showError(errorCode: number): MatSnackBarRef<SimpleSnackBar> {
    return this.open(new TranslatableString(`errors.${errorCode.toString()}`), 'warn', null, {
      duration: 5000
    });
  }

}

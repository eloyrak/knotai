import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { Comment, IComment } from '../../../backend/models/comment';
import { User } from '../../../backend/models/user';
import { CurrentUserFactory } from '../../../backend/auth/current-user';
import { FormControl, Validators } from '@angular/forms';
import { TranslatableString } from '../../models/translatable-string/translatable-string';
import { MenuItem } from '../buttons/menu-button/menu-button.component';
import { filter, finalize, switchMap, tap } from 'rxjs/operators';
import { DialogService } from '../../services/dialog/dialog.service';
import { EPortfolioService } from '../../../e-portfolio/services/e-portfolio/e-portfolio.service';
import { ToastService } from '../../services/toast/toast.service';
import { EllipsisDirective } from 'ngx-ellipsis';
import { HttpErrorResponse } from '@angular/common/http';

export declare type CommentTheme = 'light' | 'dark';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CommentComponent implements OnInit, OnDestroy {

  @ViewChild('ell', {static: false}) ell: EllipsisDirective;
  @ViewChild('input', {static: false}) input: ElementRef;

  @Input() comment: Comment;
  @Input() loading: boolean;
  @Input() theme: CommentTheme = 'light';
  @Input() autofocus: boolean;

  @Output() remove = new EventEmitter();
  @Output() add = new EventEmitter<string>();

  public actions: MenuItem<Comment>[] = [
    {
      label: new TranslatableString('edit'),
      icon: 'pencil',
      color: 'primary',
      action: () => this.editComment()
    },
    {
      label: new TranslatableString('delete'),
      icon: 'trash',
      action: () => this.deleteComment()
    }
  ];

  public author: User;
  public isOwnComment: boolean;
  public textControl: FormControl;
  public textExpanded: boolean;
  public mode: 'view' | 'edit' = 'view';

  private updateInterval: NodeJS.Timeout;

  constructor(
    private currentUser: CurrentUserFactory,
    private dialog: DialogService,
    private portfolioService: EPortfolioService,
    private toast: ToastService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.isOwnComment = this.comment && this.comment.author.id === this.currentUser.id;
    this.textControl = new FormControl('', Validators.required);
    if (!this.comment) {
      this.currentUser.user$.subscribe(user => this.author = user);
      this.mode = 'edit';
    } else {
      this.author = this.comment.author;
      setTimeout(() => this.ell.applyEllipsis(), 0);
    }
    if (this.autofocus) {
      this.focusOnInput();
    }
    // this.updateInterval = setInterval(() => this.changeDetectorRef.detectChanges(), 3e4);
  }

  ngOnDestroy() {
    clearInterval(this.updateInterval);
    this.changeDetectorRef.detach();
  }

  public cancelEdit() {
    this.textControl.reset();
    if (this.comment) {
      this.mode = 'view';
    }
  }

  public editComplete() {
    if (this.textControl.invalid || this.textControl.pristine) {
      return false;
    }
    this.comment.comment = this.textControl.value.trim();
    this.loading = true;
    const _comment: IComment = {
      id: this.comment.id,
      comment: this.comment.comment
    };
    this.cancelEdit();
    this.portfolioService.addComment(_comment)
      .pipe(finalize(() => this.loading = false))
      .subscribe((modifiedComment: Comment) => {
        this.toast.open(new TranslatableString('toasts.savedOk'));
      }, (error: HttpErrorResponse) => {
        this.toast.showError(error.status);
      });
  }

  public addComplete() {
    if (this.textControl.invalid) {
      return false;
    }
    this.add.emit(this.textControl.value.trim());
    this.cancelEdit();
    this.focusOnInput();
  }

  public onCommentExpand() {
    this.textExpanded = !this.textExpanded;
    setTimeout(() => this.ell.applyEllipsis(), 0);
  }

  private editComment() {
    this.textControl.setValue(this.comment.comment);
    this.mode = 'edit';
    this.focusOnInput();
  }

  private focusOnInput() {
    setTimeout(() => (this.input.nativeElement as HTMLInputElement).focus(), 0);
  }

  private deleteComment() {
    this.dialog.simple({
      title: new TranslatableString('item.comments.delete.title'),
      text: new TranslatableString('item.comments.delete.text'),
      primaryAction: new TranslatableString('delete'),
      secondaryAction: new TranslatableString('cancel'),
      icon: 'trash'
    }).afterClosed().pipe(
      filter((_delete: boolean) => _delete),
      tap(() => this.loading = true),
      switchMap(() => this.portfolioService.removeComment(this.comment)),
      finalize(() => this.loading = false)
    ).subscribe(() => {
      this.remove.emit();
    }, (error: HttpErrorResponse) => {
      this.toast.showError(error.status);
    });
  }

}

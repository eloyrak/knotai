import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../../backend/models/playlist';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss']
})
export class PlaylistsListComponent implements OnInit {

  public static readonly pageSize = 20;

  @Input() playlists: Playlist[] = [];
  @Input() isLastPage = true;
  @Input() loading = false;
  @Input() showAdd = false;
  @Input() selection: boolean;
  @Input() smallCards: boolean;
  @Input() cardElements: 'items' | 'subs' = 'subs';

  @Output() add = new EventEmitter();
  @Output() nextPage = new EventEmitter();
  @Output() selected = new EventEmitter<Playlist>();

  constructor() {}

  ngOnInit() {}

  public onAdd() {
    this.add.emit();
  }

  public onNextPage() {
    this.nextPage.emit();
  }

  public cardSelected(event: MouseEvent, playlist: Playlist) {
    if (this.selected) {
      event.preventDefault();
      event.stopPropagation();
      this.selected.emit(playlist);
    }
  }

}

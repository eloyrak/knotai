import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { TranslatableString } from '../../../models/translatable-string/translatable-string';
import { FormControl, Validators } from '@angular/forms';

export const INPUT_DIALOG_CONFIG: MatDialogConfig = {
  panelClass: 'simple-dialog',
  autoFocus: true
};

export interface InputDialogContent {
  title: TranslatableString;
  text?: TranslatableString;
  label?: TranslatableString;
  type?: 'input' | 'textarea';
  defaultValue?: string;
  primaryAction: TranslatableString;
  secondaryAction?: TranslatableString;
  icon?: string;
}

@Component({
  selector: 'app-input-dialog',
  templateUrl: './input-dialog.component.html',
  styleUrls: ['./input-dialog.component.scss']
})
export class InputDialogComponent implements OnInit {

  public input: FormControl;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: InputDialogContent
  ) {}

  ngOnInit() {
    this.input = new FormControl(this.data.defaultValue || '', Validators.required);
  }

}

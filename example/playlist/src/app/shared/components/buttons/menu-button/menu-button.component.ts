import { Component, Input, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material';
import { TranslatableString } from '../../../models/translatable-string/translatable-string';

export interface MenuItem<T> {
  label: TranslatableString;
  icon?: string;
  color?: ThemePalette;
  action?: (T) => void;
  callbackParam?: T;
  disabled?: boolean;
}

@Component({
  selector: 'app-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.scss']
})
export class MenuButtonComponent implements OnInit {

  @Input() actions: MenuItem<any>[];
  @Input() square: boolean;

  constructor() {}

  ngOnInit() {}

}

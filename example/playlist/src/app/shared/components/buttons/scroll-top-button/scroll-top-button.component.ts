import { AfterViewInit, Component, Input } from '@angular/core';
import { fromEvent } from 'rxjs';
import { distinctUntilChanged, map, pairwise, throttleTime } from 'rxjs/operators';

@Component({
  selector: 'app-scroll-top-button',
  templateUrl: './scroll-top-button.component.html',
  styleUrls: ['./scroll-top-button.component.scss']
})
export class ScrollTopButtonComponent implements AfterViewInit {

  public show: boolean;

  constructor() {}

  ngAfterViewInit() {
    fromEvent(window, 'scroll').pipe(
      throttleTime(50),
      map(() => document.documentElement.scrollTop),
      distinctUntilChanged(),
      pairwise()
    ).subscribe(([y1, y2]: [number, number]) => {
      this.show = (y2 > y1) && (y2 > window.innerHeight / 1.5);
    });
  }

  public scrollTop() {
    if (document.documentElement.scrollTo) {
      document.documentElement.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    } else {
      document.documentElement.scrollTop = 0;
    }
  }

}

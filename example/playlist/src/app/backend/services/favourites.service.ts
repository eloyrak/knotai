import { Injectable } from '@angular/core';
import { BackendService } from './base/backend.service';
import { Favourite, FavouriteFactoryService } from '../models/favourite';
import { Restangular } from 'ngx-restangular';

@Injectable()
export class FavouritesService extends BackendService<Favourite> {

  constructor(
    protected restangular: Restangular,
    protected factory: FavouriteFactoryService
  ) {
    super(restangular);
  }

  protected get resource() {
    return Favourite._resource;
  }

  protected get class() {
    return Favourite;
  }

}

import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import { BackendService } from './base/backend.service';
import { Inappropriate, InappropriateFactoryService } from '../models/inappropriate';

@Injectable()
export class InappropriateService extends BackendService<Inappropriate> {

  constructor(
    protected restangular: Restangular,
    protected factory: InappropriateFactoryService
  ) {
    super(restangular);
  }

  protected get resource() {
    return Inappropriate._resource;
  }

  protected get class() {
    return Inappropriate;
  }

}

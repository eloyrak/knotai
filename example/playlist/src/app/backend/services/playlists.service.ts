import { Injectable } from '@angular/core';
import { BackendService } from './base/backend.service';
import { Playlist, PlaylistFactoryService } from '../models/playlist';
import { Restangular } from 'ngx-restangular';

@Injectable()
export class PlaylistsService extends BackendService<Playlist> {

  constructor(
    protected restangular: Restangular,
    protected factory: PlaylistFactoryService
  ) {
    super(restangular);
  }

  protected get resource() {
    return Playlist._resource;
  }

  protected get class() {
    return Playlist;
  }

}

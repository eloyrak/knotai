import { Injectable } from '@angular/core';
import { BackendService } from './base/backend.service';
import { Item, ItemFactoryService } from '../models/item';
import { Restangular } from 'ngx-restangular';
import { Observable } from 'rxjs';

@Injectable()
export class ItemsService extends BackendService<Item> {

  constructor(
    protected restangular: Restangular,
    protected factory: ItemFactoryService
  ) {
    super(restangular);
  }

  protected get resource() {
    return Item._resource;
  }

  protected get class() {
    return Item;
  }

  public getFavouritesByUser(id: string, pageNumber?: number, criterias: Object = {}): Observable<Item[]> {
    return (
      this.restangular
        .all(this.resource)
        .one('user', id)
        .all('favourites')
        .getList(BackendService.makeCriterias(criterias, pageNumber))
    );
  }

  public removeInappropriate(item: Item): Observable<any> {
    return this.restangular.one(this.resource, item.id).one('inappropriate').remove();
  }

}

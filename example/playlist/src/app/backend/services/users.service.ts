import { Injectable } from '@angular/core';
import { BackendService } from './base/backend.service';
import { User, UserFactoryService } from '../models/user';
import { Restangular } from 'ngx-restangular';

@Injectable()
export class UsersService extends BackendService<User> {

  constructor(
    protected restangular: Restangular,
    protected factory: UserFactoryService
  ) {
    super(restangular);
  }

  protected get resource() {
    return User._resource;
  }

  protected get class() {
    return User;
  }

}

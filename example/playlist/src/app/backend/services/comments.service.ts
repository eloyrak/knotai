import { Injectable } from '@angular/core';
import { BackendService } from './base/backend.service';
import { Comment, CommentFactoryService } from '../models/comment';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class CommentsService extends BackendService<Comment> {

  constructor(
    protected restangular: Restangular,
    protected factory: CommentFactoryService
  ) {
    super(restangular);
  }

  protected get resource() {
    return Comment._resource;
  }

  protected get class() {
    return Comment;
  }

}

import { AuthData } from './auth-data';
import { Injectable } from '@angular/core';
import { JwtToken } from '../models/jwt/jwt-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private auth: AuthData = new AuthData();

  constructor() {}

  public setAuth(authData: AuthData): void {
    this.auth = authData;
  }

  public getAuth(): AuthData {
    return this.auth;
  }

  public getJwtToken(): JwtToken {
    return this.auth.jwtToken;
  }

  public isAuthenticated(): boolean {
    if (this.auth.jwtToken) {
      const jwtTokenDecode = this.auth.jwtToken.decode();
      return (jwtTokenDecode.exp && jwtTokenDecode.user_id != null);
    }
    return false;
  }

}

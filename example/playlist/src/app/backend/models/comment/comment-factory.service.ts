import { Injectable } from '@angular/core';
import { IFactory } from '../../services/base/IFactory';
import { Comment } from './comment';
import { IComment } from './icomment';
import { UserFactoryService } from '../user';

@Injectable({
  providedIn: 'root'
})
export class CommentFactoryService implements IFactory<Comment> {

  constructor() {}

  build(data: IComment): Comment {

    if (data.author) {
      data.author = new UserFactoryService().build(data.author);
    }

    return new Comment(data);

  }

}

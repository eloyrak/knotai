export interface ContextualMenu {
  username: string;
  name: string;
  surname: string;
  account?: ContextualMenuLink,
  logout?: ContextualMenuLink,
  news?: ContextualMenuLink,
  notifications?: ContextualMenuLink,
  userManual?: ContextualMenuLink
}

interface ContextualMenuLink {
  url: string;
  label: string;
  target?: '_self' | '_blank';
}

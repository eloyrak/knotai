export { IInappropriate } from './iinappropriate';
export { Inappropriate } from './inappropriate';
export { InappropriateFactoryService } from './inappropriate-factory.service';

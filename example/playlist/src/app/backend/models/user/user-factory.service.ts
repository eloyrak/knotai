import { Injectable } from '@angular/core';
import { IFactory } from '../../services/base/IFactory';
import { User } from './user';
import { IUser } from './iuser';

@Injectable({
  providedIn: 'root'
})
export class UserFactoryService implements IFactory<User> {

  constructor() {}

  build(data: IUser): User {

    return new User(data);

  }

}

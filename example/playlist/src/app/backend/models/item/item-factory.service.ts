import { Injectable } from '@angular/core';
import { IFactory } from '../../services/base/IFactory';
import { Item } from './item';
import { PlaylistFactoryService } from '../playlist';
import { UserFactoryService } from '../user';
import { IItem } from './iitem';
import { InappropriateFactoryService } from '../inappropriate';

@Injectable({
  providedIn: 'root'
})
export class ItemFactoryService implements IFactory<Item> {

  constructor() {}

  build(data: IItem): Item {

    if (data.playlist) {
      data.playlist = new PlaylistFactoryService().build(data.playlist);
    }

    if (data.author) {
      data.author = new UserFactoryService().build(data.author);
    }

    if (data.inappropriate) {
      data.inappropriate = new InappropriateFactoryService().build(data.inappropriate);
    }

    return new Item(data);

  }

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CurrentUserFactory } from '../../../backend/auth/current-user';

@Component({
  selector: 'app-recycle-bin',
  templateUrl: './recycle-bin.component.html',
  styleUrls: ['./recycle-bin.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RecycleBinComponent implements OnInit {

  public currentUserId: string;

  constructor(
    private currentUser: CurrentUserFactory
  ) {}

  ngOnInit() {
    this.currentUserId = this.currentUser.id;
  }

}

import { Component, OnInit } from '@angular/core';
import { Playlist } from '../../../../backend/models/playlist';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { CurrentUserFactory } from '../../../../backend/auth/current-user';
import { TabIcon } from '../../../../shared/components/tabs/tabs-icons/tabs-icons.component';
import { TranslatableString } from '../../../../shared/models/translatable-string/translatable-string';
import { MenuItem } from '../../../../shared/components/buttons/menu-button/menu-button.component';
import { SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import { PlaylistsAddComponent, PlaylistsAddData } from '../playlists-add/playlists-add.component';
import { ToastService } from '../../../../shared/services/toast/toast.service';
import { BehaviorSubject } from 'rxjs';
import { filter, finalize, switchMap, tap } from 'rxjs/operators';
import { DialogService } from '../../../../shared/services/dialog/dialog.service';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { ItemsAddComponent, ItemsAddData } from '../../items/items-add/items-add.component';
import { Item } from '../../../../backend/models/item';
import { Subscription } from '../../../../backend/models/subscription';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-playlist-detail',
  templateUrl: './playlist-detail.component.html',
  styleUrls: ['./playlist-detail.component.scss']
})
export class PlaylistDetailComponent implements OnInit {

  public actions: MenuItem<Playlist>[];

  public navLinks: TabIcon[] = [];
  public playlist: Playlist;
  public isOwnPlaylist: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private currentUser: CurrentUserFactory,
    private sidenav: SidenavService,
    private toast: ToastService,
    private dialog: DialogService,
    private portfolioService: EPortfolioService
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: { playlist: Playlist }) => {
      if (data.playlist) {
        this.playlist = data.playlist;
        this.isOwnPlaylist = this.playlist.author.id === this.currentUser.id;
        this.createTabs();
        this.createMenu();
      }
    });
  }

  public back() {
    this.location.back();
  }

  private createTabs() {
    this.navLinks = [
      {
        path: 'items',
        icon: 'collections',
        amount: this.playlist.numberItems,
        label: new TranslatableString('elements', {count: 0})
      },
      {
        path: 'subscribers',
        icon: 'users',
        amount: this.playlist.numberSubscribers,
        label: new TranslatableString('subscribers', {count: 0})
      }
    ]
  }

  private createMenu() {
    if (this.isOwnPlaylist) {
      this.actions = [
        {
          label: new TranslatableString('playlist.actions.addItem'),
          icon: 'plus',
          color: 'primary',
          action: () => this.addItem()
        },
        {
          label: new TranslatableString('edit'),
          icon: 'pencil',
          color: 'primary',
          action: () => this.editPlaylist()
        },
        {
          label: new TranslatableString('delete'),
          icon: 'cross',
          action: () => this.deletePlaylist()
        }
      ];
    } else {
      this.actions = [
        {
          label: new TranslatableString(`playlist.actions.${this.playlist.idSubscription ? 'unsubscribe' : 'subscribe'}`),
          icon: this.playlist.idSubscription ? 'less-circle' : 'plus',
          color: 'primary',
          action: () => this.changeSubscription()
        }
      ]
    }
  }

  private addItem() {
    this.sidenav.open(ItemsAddComponent, <ItemsAddData>{
      initialPlaylist: this.playlist
    }).pipe(filter((addedItem: Item) => !!addedItem))
      .subscribe((addedItem: Item) => {
        this.updatePlaylist(addedItem.playlist);
      });
  }

  private editPlaylist() {
    this.sidenav.open(PlaylistsAddComponent, <PlaylistsAddData>{
      currentPlaylist: this.playlist
    }).pipe(filter((modifiedPlaylist: Playlist) => !!modifiedPlaylist))
      .subscribe((modifiedPlaylist: Playlist) => {
        this.toast.open(new TranslatableString('toasts.savedOk'));
        this.updatePlaylist(modifiedPlaylist);
      })
  }

  private deletePlaylist() {
    this.dialog.simple({
      title: new TranslatableString('playlist.delete.title'),
      text: new TranslatableString('playlist.delete.text'),
      primaryAction: new TranslatableString('delete'),
      secondaryAction: new TranslatableString('cancel'),
      icon: 'trash'
    }).afterClosed().pipe(
      filter((_delete: boolean) => _delete),
      tap(() => this.dialog.openLoader()),
      switchMap(() => this.portfolioService.removePlaylist(this.playlist)),
      finalize(() => this.dialog.closeLoader())
    ).subscribe(() => {
      this.router.navigate(['/']);
      this.toast.open(
        new TranslatableString('toasts.deletedPlaylist', {name: this.playlist.title}),
        'success',
        new TranslatableString('close')
      );
    }, (error: HttpErrorResponse) => {
      this.toast.showError(error.status);
    });
  }

  private changeSubscription() {
    if (this.playlist.idSubscription) {
      this.portfolioService.unSubscribePlaylist(this.playlist).subscribe(() => {
        this.updateSubscriptionData();
      });
    } else {
      this.portfolioService.subscribePlaylist(this.playlist).subscribe((subscription: Subscription) => {
        this.updateSubscriptionData(subscription);
      });
    }
  }

  private updateSubscriptionData(subscription?: Subscription) {
    this.playlist.idSubscription = subscription ? subscription.id : null;
    this.playlist.numberSubscribers = this.playlist.numberSubscribers + (subscription ? 1 : -1);
    this.createMenu();
    this.createTabs();
    this.toast.open(
      new TranslatableString(`toasts.${subscription ? 'subscribed' : 'unsubscribed'}`),
      'success',
      new TranslatableString('undo')
    ).onAction().subscribe(() => this.changeSubscription());
  }

  private updatePlaylist(playlist: Playlist) {
    if (playlist.id === this.playlist.id) {
      (this.route.data as BehaviorSubject<any>).next({
        ...(this.route.data as BehaviorSubject<any>).value,
        playlist: Object.assign({}, this.playlist, playlist)
      });
    }
  }

}

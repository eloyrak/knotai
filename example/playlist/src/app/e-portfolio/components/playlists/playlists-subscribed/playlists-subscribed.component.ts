import { Component, OnInit } from '@angular/core';
import { Playlist } from '../../../../backend/models/playlist';
import { SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { PlaylistsListComponent } from '../../../../shared/components/lists/playlists-list/playlists-list.component';
import { finalize } from 'rxjs/operators';
import { RestangularCollection } from '../../../../backend/services/base/backend.service';

@Component({
  selector: 'app-playlists-subscribed',
  templateUrl: './playlists-subscribed.component.html',
  styleUrls: ['./playlists-subscribed.component.scss']
})
export class PlaylistsSubscribedComponent implements OnInit {

  public playlists: Playlist[] = [];
  public page = 0;
  public isLastPage = true;
  public loading = false;

  constructor(
    private sidenavService: SidenavService,
    private portfolioService: EPortfolioService
  ) {}

  ngOnInit() {
    this.getPlaylists();
  }

  public getPlaylists() {
    this.loading = true;
    this.portfolioService.getSubscribedPlaylists(++this.page, PlaylistsListComponent.pageSize)
      .pipe(finalize(() => this.loading = false))
      .subscribe(playlists => {
        this.playlists.push(...playlists);
        this.isLastPage = this.playlists.length >= (playlists as RestangularCollection<Playlist>).totalItems;
      });
  }

}

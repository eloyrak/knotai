import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsReviewUsersComponent } from './items-review-users.component';

describe('ItemsReviewUsersComponent', () => {
  let component: ItemsReviewUsersComponent;
  let fixture: ComponentFixture<ItemsReviewUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsReviewUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsReviewUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

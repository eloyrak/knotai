import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Item } from '../../../../backend/models/item';
import { MenuItem } from '../../../../shared/components/buttons/menu-button/menu-button.component';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { ToastService } from '../../../../shared/services/toast/toast.service';
import { ItemsAddComponent, ItemsAddData } from '../items-add/items-add.component';
import { SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import { TranslatableString } from '../../../../shared/models/translatable-string/translatable-string';
import { DialogService } from '../../../../shared/services/dialog/dialog.service';
import { filter, finalize, switchMap, tap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { CurrentUserFactory } from '../../../../backend/auth/current-user';
import { Role } from '../../../../backend/services/config/BackendConstants';
import { ItemOpinionsAction, ItemOpinionsComponent, ItemOpinionsData } from '../item-opinions/item-opinions.component';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ItemDetailComponent implements OnInit {

  public actions: MenuItem<Item>[];
  public currentItem: Item;
  public isOwnItem: boolean;
  public isAdmin: boolean;
  public body: SafeHtml;

  constructor(
    private route: ActivatedRoute,
    private portfolioService: EPortfolioService,
    private toast: ToastService,
    private sidenav: SidenavService,
    private dialog: DialogService,
    private location: Location,
    private currentUser: CurrentUserFactory,
    private _sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.route.data.subscribe((data: { item: Item }) => {
      if (data.item) {
        this.currentItem = data.item;
        this.isOwnItem = this.currentItem.author && this.currentItem.author.id === this.currentUser.id;
        this.isAdmin = this.currentUser.isRole(Role.ADMIN);
        this.createMenu();
        this.getItemBody();
      }
    });
  }

  private getItemBody() {
    this.body = this._sanitizer.bypassSecurityTrustHtml(this.currentItem.description);
  }

  private createMenu() {
    this.actions = [];
    if ((this.isAdmin && this.currentItem.inappropriate) || (this.isOwnItem && this.currentItem.inappropriate && !this.currentItem.inappropriate.authorReviewed)) {
      this.actions.push({
        label: new TranslatableString('item.actions.opinions'),
        icon: 'eye',
        color: 'primary',
        action: () => this.viewOpinions()
      });
    }
    if (this.isOwnItem) {
      this.actions.push({
        label: new TranslatableString('edit'),
        icon: 'pencil',
        color: 'primary',
        action: () => this.editItem()
      });
    }
    if (!this.isOwnItem) {
      this.actions.push({
        label: new TranslatableString(`item.actions.${this.currentItem.inappropriate ? 'marked' : 'mark'}`),
        icon: 'flag',
        color: 'primary',
        disabled: !!this.currentItem.inappropriate,
        action: () => this.markInappropriate()
      });
    }
    if (this.isOwnItem || this.isAdmin) {
      this.actions.push({
        label: new TranslatableString('delete'),
        icon: 'cross',
        action: () => this.deleteItem()
      });
    }
  }

  private goToComments(el: HTMLElement) {
    el.scrollIntoView({
      block: 'start',
      inline: 'nearest',
      behavior: 'smooth'
    });
  }

  private editItem() {
    this.sidenav.open(ItemsAddComponent, <ItemsAddData>{
      initialPlaylist: this.currentItem.playlist,
      currentItem: this.currentItem
    }).subscribe(modifiedItem => {
      if (modifiedItem) {
        this.toast.open(new TranslatableString('toasts.savedOk'));
        Object.assign(this.currentItem, modifiedItem);
        this.getItemBody();
      }
    });
  }

  private deleteItem() {
    this.dialog.simple({
      title: new TranslatableString('item.delete.title'),
      text: new TranslatableString('item.delete.text'),
      primaryAction: new TranslatableString('delete'),
      secondaryAction: new TranslatableString('cancel'),
      icon: 'trash'
    }).afterClosed().pipe(
      filter((_delete: boolean) => _delete),
      tap(() => this.dialog.openLoader()),
      switchMap(() => this.portfolioService.removeItem(this.currentItem)),
      finalize(() => this.dialog.closeLoader())
    ).subscribe(() => {
      this.toast.open(new TranslatableString('toasts.deletedItem'), 'success', new TranslatableString('close'));
      setTimeout(() => window.close(), 500);
    }, (error: HttpErrorResponse) => {
      this.toast.showError(error.status);
    });
  }

  private markInappropriate() {
    this.dialog.markAsInappropriate().pipe(
      filter((comment: string) => !!comment),
      tap(() => this.dialog.openLoader()),
      switchMap((comment: string) => this.portfolioService.markItemInappropriate(this.currentItem, comment)),
      finalize(() => this.dialog.closeLoader())
    ).subscribe(inappropriateEntity => {
      this.toast.open(new TranslatableString('toasts.markInappropriate'));
      this.currentItem.inappropriate = {id: inappropriateEntity.id};
      this.createMenu();
    }, (error: HttpErrorResponse) => {
      this.toast.showError(error.status);
    });
  }

  private viewOpinions() {
    this.sidenav.open(ItemOpinionsComponent, <ItemOpinionsData>{item: this.currentItem})
      .subscribe((action: ItemOpinionsAction) => {
        switch (action) {
          case ItemOpinionsAction.UNMARKED:
            if (this.isAdmin) {
              this.currentItem.inappropriate = null;
            } else {
              this.currentItem.inappropriate.authorReviewed = true;
            }
            this.createMenu();
            break;
          case ItemOpinionsAction.DELETED:
            this.location.back();
            this.toast.open(new TranslatableString('toasts.deletedItem'), 'success', new TranslatableString('close'));
            break;
        }
      });
  }

}

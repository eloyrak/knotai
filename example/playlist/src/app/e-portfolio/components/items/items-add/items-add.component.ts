import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogService } from '../../../../shared/services/dialog/dialog.service';
import { SIDENAV_DATA, SidenavService } from '../../../../shared/services/sidenav/sidenav.service';
import { Playlist } from '../../../../backend/models/playlist';
import {
  ImageSelectorData,
  PanelImageSelectorComponent
} from '../../../../shared/components/panels/panel-image-selector/panel-image-selector.component';
import { EPortfolioService } from '../../../services/e-portfolio/e-portfolio.service';
import { IItem, Item } from '../../../../backend/models/item';
import { PlaylistSelectorComponent } from '../../playlists/playlist-selector/playlist-selector.component';
import BAEditor from '../../../../shared/libraries/ckeditor-build-ba/build/ckeditor.js';
import { finalize } from 'rxjs/operators';
import { MatHorizontalStepper } from '@angular/material';
import { PlaylistsAddComponent, PlaylistsAddData } from '../../playlists/playlists-add/playlists-add.component';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastService } from '../../../../shared/services/toast/toast.service';

export interface ItemsAddData {
  initialPlaylist: Playlist;
  currentItem?: Item;
  link?: string;
}

@Component({
  selector: 'app-items-add',
  templateUrl: './items-add.component.html',
  styleUrls: ['./items-add.component.scss']
})
export class ItemsAddComponent implements OnInit {

  @ViewChild('stepper', {static: true}) stepper: MatHorizontalStepper;

  public url: FormControl;
  public form: FormGroup;
  public preview: string;
  public playlist: Playlist;

  public Editor = BAEditor;

  constructor(
    private portfolioService: EPortfolioService,
    private fb: FormBuilder,
    private dialog: DialogService,
    private sidenav: SidenavService,
    private toast: ToastService,
    @Inject(SIDENAV_DATA) public data: ItemsAddData
  ) {
    if (data) {
      this.playlist = data.initialPlaylist;
    }
  }

  ngOnInit() {
    this.url = new FormControl('', Validators.required);
    this.form = this.fb.group({
      avatar: null,
      title: [this.data.currentItem ? this.data.currentItem.title : '', Validators.required],
      idPlaylist: [this.playlist && this.playlist.id || '', Validators.required],
      description: this.data.currentItem ? this.data.currentItem.description : '',
      link: this.data.currentItem ? this.data.currentItem.link : ''
    });
    if (!this.playlist) {
      this.dialog.openLoader();
      this.portfolioService.getOwnPlaylists(1, 1)
        .pipe(finalize(() => this.dialog.closeLoader()))
        .subscribe(playlistArray => {
          this.updatePlaylist(playlistArray[0]);
        }, (error: HttpErrorResponse) => {
          this.toast.showError(error.status);
        });
    }
    if (this.data.link) {
      this.stepper.selectedIndex = 1;
      this.url.setValue(this.data.link);
      this.form.get('link').setValue(this.data.link);
      this.getLinkInfo();
    }
    if (this.data.currentItem) {
      this.stepper.selectedIndex = 1;
      this.preview = this.data.currentItem.avatar;
    }
    this.form.controls.avatar.valueChanges.subscribe(blob => {
      const reader = new FileReader();
      reader.onload = _ => this.preview = reader.result.toString();
      reader.readAsDataURL(blob);
    });
  }

  public getLinkInfo() {
    this.dialog.openLoader();
    this.portfolioService.proccessItemLink(this.url.value)
      .pipe(finalize(() => this.dialog.closeLoader()))
      .subscribe(item => {
        for (const key of Object.keys(this.form.controls)) {
          if (item[key]) {
            this.form.controls[key].setValue(item[key]);
          }
        }
        if (item.image) {
          const parts = item.image.split(',');
          const binary = atob(parts[1]);
          const array = [];
          for (let i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
          }
          this.form.get('avatar').setValue(new Blob([new Uint8Array(array)], {
            type: parts[0].match(/data:(.*?)[?;]/)[1]
          }));
        }
        if (this.stepper.selectedIndex === 0) {
          this.stepper.next();
        }
      }, (error: HttpErrorResponse) => {
        this.toast.showError(error.status);
        setTimeout(() => this.sidenav.close(), 3000);
      });
  }

  public editCover() {
    this.sidenav.open(PanelImageSelectorComponent, <ImageSelectorData>{
      control: this.form.get('avatar')
    });
  }

  public selectPlaylist() {
    this.sidenav.open(PlaylistSelectorComponent, {}, 'small').subscribe((selectedPlaylist: Playlist) => {
      this.updatePlaylist(selectedPlaylist);
    });
  }

  public addPlaylist() {
    this.sidenav.open(PlaylistsAddComponent, <PlaylistsAddData>{}).subscribe((createdPlaylist: Playlist) => {
      this.updatePlaylist(createdPlaylist);
    });
  }

  public submit() {
    if (this.data.currentItem && this.form.pristine) {
      this.sidenav.close();
      return;
    }
    this.dialog.openLoader();
    const item: IItem = this.form.value as IItem;
    if (item.idPlaylist) {
      item.idPlaylist = `/api/${Playlist._resource}/${item.idPlaylist}`;
    }
    if (this.data.currentItem) {
      item.id = this.data.currentItem.id;
    }
    this.portfolioService.addItem(item)
      .pipe(finalize(() => this.dialog.closeLoader()))
      .subscribe(addedItem => {
        this.sidenav.close(addedItem);
      }, (error: HttpErrorResponse) => {
        this.toast.showError(error.status);
      });
  }

  private updatePlaylist(playlist: Playlist) {
    if (playlist) {
      this.playlist = playlist;
      this.form.get('idPlaylist').setValue(this.playlist.id || '');
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsPlaylistComponent } from './items-playlist.component';

describe('ItemsPlaylistComponent', () => {
  let component: ItemsPlaylistComponent;
  let fixture: ComponentFixture<ItemsPlaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsPlaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsPlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

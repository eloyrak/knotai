import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsFavouritesComponent } from './items-favourites.component';

describe('ItemsFavouritesComponent', () => {
  let component: ItemsFavouritesComponent;
  let fixture: ComponentFixture<ItemsFavouritesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsFavouritesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsFavouritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { NavLink } from '../../../shared/components/tabs/tabs-buttons/tabs-buttons.component';
import { TranslatableString } from '../../../shared/models/translatable-string/translatable-string';
import { CurrentUserFactory } from '../../../backend/auth/current-user';

@Component({
  selector: 'app-my-subscriptions',
  templateUrl: './my-subscriptions.component.html',
  styleUrls: ['./my-subscriptions.component.scss']
})
export class MySubscriptionsComponent implements OnInit {

  public navLinks: NavLink[] = [];

  constructor(
    private currentUser: CurrentUserFactory
  ) {}

  ngOnInit() {
    this.currentUser.relations$.subscribe(relations => {
      this.navLinks = [
        {
          path: 'items',
          label: new TranslatableString('items', {count: 0}),
          amount: relations.subscriptions && relations.subscriptions.elements.items.elements
        },
        {
          path: 'playlists',
          label: new TranslatableString('Playlists', {count: 0}),
          amount: relations.subscriptions && relations.subscriptions.elements.playlists.elements
        }
      ];
    });
  }

}

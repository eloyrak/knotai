import { Injectable } from '@angular/core';
import {
  CommentsService,
  FavouritesService,
  InappropriateService,
  ItemsService,
  PlaylistsService,
  SubscriptionsService,
  TagsService,
  UsersService
} from '../../../backend/services';
import { Observable, of } from 'rxjs';
import { Tag } from '../../../backend/models/tag';
import { IItem, Item } from '../../../backend/models/item';
import { IPlaylist, Playlist } from '../../../backend/models/playlist';
import { CurrentUserFactory } from '../../../backend/auth/current-user';
import { tap } from 'rxjs/operators';
import { User } from '../../../backend/models/user';
import { Subscription } from '../../../backend/models/subscription';
import { IInappropriate, Inappropriate } from '../../../backend/models/inappropriate';
import { Comment, IComment } from '../../../backend/models/comment';
import { Role } from '../../../backend/services/config/BackendConstants';
import { Sort } from '@angular/material';
import { Favourite } from '../../../backend/models/favourite';

@Injectable({
  providedIn: 'root'
})
export class EPortfolioService {

  constructor(
    private tagsService: TagsService,
    private playlistsService: PlaylistsService,
    private itemsService: ItemsService,
    private currentUser: CurrentUserFactory,
    private usersService: UsersService,
    private subscriptionService: SubscriptionsService,
    private inappropriateService: InappropriateService,
    private commentsService: CommentsService,
    private favouritesService: FavouritesService
  ) {
  }

  public getTags(name: string, size: number): Observable<Tag[]> {
    name = name || '';
    return name ? this.tagsService.getAll(1, {name, size}) : of([]);
  }

  public getPlaylistsByUser(id: string, page: number, size?: number, sort?: Sort): Observable<Playlist[]> {
    return this.playlistsService.getAllBy('author', id, page, {size, sort});
  }

  public getOwnPlaylists(page: number, size?: number, sort?: Sort): Observable<Playlist[]> {
    return this.getPlaylistsByUser(this.currentUser.id, page, size, sort);
  }

  public getSubscribedPlaylists(page: number, size?: number, sort?: Sort): Observable<Playlist[]> {
    return this.playlistsService.getAllBy('user', this.currentUser.id, page, {size, sort});
  }

  public addPlaylist(playlist: IPlaylist): Observable<Playlist> {
    return this.playlistsService.addFormData(playlist as Playlist)
      .pipe(tap(() => {
        if (!playlist.id) {
          this.currentUser.reload()
        }
      }));
  }

  public getPlaylist(id: string): Observable<Playlist> {
    return this.playlistsService.get(id);
  }

  public getSubscribedItems(page: number, size?: number, sort?: Sort): Observable<Item[]> {
    return this.itemsService.getAllBy('user', this.currentUser.id, page, {size, sort});
  }

  public getPlaylistItems(playlistId: string, page: number, size?: number, sort?: Sort): Observable<Item[]> {
    return this.itemsService.getAllBy('playlist', playlistId, page, {size, sort});
  }

  public getItemById(id: string): Observable<Item> {
    return this.itemsService.get(id);
  }

  public getPlaylistSubscribers(id: string, page: number, size?: number, sort?: Sort): Observable<User[]> {
    return this.usersService.getAllBy('playlist', id, page, {size, sort});
  }

  public addItem(item: IItem): Observable<Item> {
    return this.itemsService.addFormData(item as Item);
  }

  public proccessItemLink(url: string): Observable<Item> {
    return this.itemsService.get('linkpreview', {url});
  }

  public removeItem(item: Item): Observable<any> {
    return this.itemsService.remove(item)
      .pipe(tap(() => this.currentUser.reload()));
  }

  public removePlaylist(playlist: Playlist): Observable<any> {
    return this.playlistsService.remove(playlist)
      .pipe(tap(() => this.currentUser.reload()));
  }

  public subscribePlaylist(playlist: Playlist): Observable<Subscription> {
    return this.subscriptionService.add(new Subscription({idPlaylist: playlist.id}))
      .pipe(tap(() => this.currentUser.reload()));
  }

  public unSubscribePlaylist(playlist: Playlist): Observable<any> {
    return this.subscriptionService.remove(new Subscription({id: playlist.idSubscription}))
      .pipe(tap(() => this.currentUser.reload()));
  }

  public markItemInappropriate(item: Item, comment: string): Observable<Inappropriate> {
    return this.inappropriateService.add({
      idItem: item.id,
      comment
    });
  }

  public markItemAppropriate(inappropriateEntity: IInappropriate): Observable<any> {
    if (this.currentUser.isRole(Role.ADMIN)) {
      return this.inappropriateService.remove(inappropriateEntity as Inappropriate)
        .pipe(tap(() => this.currentUser.reload()));
    }
    return this.inappropriateService.update({
      id: inappropriateEntity.id,
      authorReviewed: true
    }).pipe(tap(() => this.currentUser.reload()));
  }

  public removeInappropriateItem(item: IItem): Observable<any> {
    return this.itemsService.removeInappropriate(item as Item)
      .pipe(tap(() => this.currentUser.reload()));
  }

  public getFavouriteItems(userId: string, page: number, size?: number, sort?: Sort): Observable<Item[]> {
    return this.itemsService.getFavouritesByUser(userId, page, {size, sort});
  }

  public getItemComments(itemId: string, page: number, size?: number, sort?: Sort): Observable<Comment[]> {
    return this.commentsService.getAllBy('item', itemId, page, {size, sort});
  }

  public addComment(comment: IComment): Observable<Comment> {
    return (
      comment.id ?
        this.commentsService.update(comment as Comment) :
        this.commentsService.add(comment as Comment)
    );
  }

  public removeComment(comment: IComment): Observable<any> {
    return this.commentsService.remove(comment as Comment);
  }

  public getInappropriateItems(userId: string, page: number, size?: number, sort?: Sort): Observable<Item[]> {
    return (
      userId ?
        this.itemsService.getAllBy('inappropriate/author', userId, page, {size, sort}) :
        this.itemsService.getAllBy('inappropriate', '', page, {size, sort})
    );
  }

  public getItemOpinions(idItem: string, page: number, size?: number, sort?: Sort): Observable<Inappropriate[]> {
    return this.inappropriateService.getAll(page, {size, sort, idItem});
  }

  public getDeletedItems(userId: string, page: number, size?: number, sort?: Sort): Observable<Item[]> {
    return (
      userId ?
        this.itemsService.getAllBy('inappropriate/deleted/author', userId, page, {size, sort}) :
        this.itemsService.getAllBy('inappropriate/deleted', '', page, {size, sort})
    );
  }

  public getOwnDeletedItems(page: number, size?: number, sort?: Sort): Observable<Item[]> {
    return this.getDeletedItems(this.currentUser.id, page, size, sort);
  }

  public markFavourite(item: IItem): Observable<Favourite> {
    const url = `/api/items/${item.id}`;
    return this.favouritesService.add({idItem: url} as Favourite)
      .pipe(tap(() => this.currentUser.reload()));
  }

  public unmarkFavourite(item: IItem): Observable<Favourite> {
    return this.favouritesService.remove({id: item.idFavourite} as Favourite)
      .pipe(tap(() => this.currentUser.reload()));
  }

}

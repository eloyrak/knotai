import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EPortfolioComponent } from './components/e-portfolio/e-portfolio.component';
import { MySubscriptionsComponent } from './components/my-subscriptions/my-subscriptions.component';
import { PlaylistDetailComponent } from './components/playlists/playlist-detail/playlist-detail.component';
import { PlaylistResolverService } from './services/playlist-resolver/playlist-resolver.service';
import { PlaylistsUserComponent } from './components/playlists/playlists-user/playlists-user.component';
import { PlaylistsSubscribedComponent } from './components/playlists/playlists-subscribed/playlists-subscribed.component';
import { ItemsSubscribedComponent } from './components/items/items-subscribed/items-subscribed.component';
import { ItemsPlaylistComponent } from './components/items/items-playlist/items-playlist.component';
import { PeoplePlaylistSubscribersComponent } from './components/people/people-playlist-subscribers/people-playlist-subscribers.component';
import { ItemDetailComponent } from './components/items/item-detail/item-detail.component';
import { UserResolverService } from './services/user-resolver/user-resolver.service';
import { ItemResolverService } from './services/item-resolver/item-resolver.service';
import { ItemsFavouritesComponent } from './components/items/items-favourites/items-favourites.component';
import { ReviewComponent } from './components/review/review.component';
import { ItemsReviewOwnComponent } from './components/items/items-review-own/items-review-own.component';
import { ItemsReviewUsersComponent } from './components/items/items-review-users/items-review-users.component';
import { RoleGuard } from '../backend/auth/role.guard';
import { Role } from '../backend/services/config/BackendConstants';

const routes: Routes = [
  {
    path: 'e-portfolio/user/:userId',
    component: EPortfolioComponent,
    resolve: {
      user: UserResolverService
    },
    children: [
      {
        path: '',
        redirectTo: 'playlists',
        pathMatch: 'full'
      },
      {
        path: 'playlists',
        component: PlaylistsUserComponent
      },
      {
        path: 'favourites',
        component: ItemsFavouritesComponent
      },
      {
        path: '**',
        redirectTo: 'playlists'
      }
    ]
  },
  {
    path: 'e-portfolio',
    component: EPortfolioComponent,
    children: [
      {
        path: '',
        redirectTo: 'playlists',
        pathMatch: 'full'
      },
      {
        path: 'playlists',
        component: PlaylistsUserComponent
      },
      {
        path: 'subscriptions',
        component: MySubscriptionsComponent,
        children: [
          {
            path: '',
            redirectTo: 'items',
            pathMatch: 'full'
          },
          {
            path: 'playlists',
            component: PlaylistsSubscribedComponent
          },
          {
            path: 'items',
            component: ItemsSubscribedComponent
          }
        ]
      },
      {
        path: 'favourites',
        component: ItemsFavouritesComponent
      },
      {
        path: 'review',
        component: ReviewComponent,
        children: [
          {
            path: '',
            redirectTo: 'own',
            pathMatch: 'full'
          },
          {
            path: 'own',
            component: ItemsReviewOwnComponent
          },
          {
            path: 'users',
            component: ItemsReviewUsersComponent,
            canActivate: [RoleGuard],
            data: {role: Role.ADMIN}
          }
        ]
      },
      {
        path: '**',
        redirectTo: 'playlists'
      }
    ]
  },
  {
    path: 'playlist/:playlistId',
    component: PlaylistDetailComponent,
    resolve: {
      playlist: PlaylistResolverService
    },
    children: [
      {
        path: '',
        redirectTo: 'items',
        pathMatch: 'full'
      },
      {
        path: 'items',
        component: ItemsPlaylistComponent
      },
      {
        path: 'subscribers',
        component: PeoplePlaylistSubscribersComponent
      }
    ]
  },
  {
    path: 'item/:itemId',
    component: ItemDetailComponent,
    resolve: {
      item: ItemResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EPortfolioRoutingModule {}

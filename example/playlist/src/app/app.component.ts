import { Component } from '@angular/core';
import {
  ActivatedRoute,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
  RouterEvent
} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { DialogService } from './shared/services/dialog/dialog.service';
import { filter } from 'rxjs/operators';
import { ApplicationsMenuItem, ContextualMenu } from './backend/models/user-menu';
import { CurrentUserFactory } from './backend/auth/current-user';
import { AuthService } from './backend/auth/auth.service';
import { AuthData } from './backend/auth/auth-data';
import { JwtToken } from './backend/models/jwt/jwt-token';
import Timer = NodeJS.Timer;

interface InjectedUserData {
  contextualMenu: ContextualMenu;
  applicationsMenu: ApplicationsMenuItem[];
  userToken: string;
  userId: string;
  urlPlay: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public title = 'nx-playlist';

  private navigationDialogTimeout: Timer = null;
  private _window: InjectedUserData = window as any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private dialog: DialogService,
    private currentUser: CurrentUserFactory,
    private auth: AuthService
  ) {

    // Init translations.
    this.initTranslations();

    // Show loader on route change.
    this.initRouteLoader();

    // Hack to scroll top when changing route.
    // TODO: Follow up scrollPositionRestoration in routing module, or check another solution.
    this.initRouteScrollTop();

    // Get injected data from window.
    this.getInjectedData();

  }

  private initTranslations() {
    this.translate.addLangs(environment.availableLangs);
    this.translate.setDefaultLang(environment.defaultLang);
    this.translate.use(environment.defaultLang);
  }

  private initRouteLoader() {
    this.router.events.subscribe((event: RouterEvent) => {
      switch (true) {
        case event instanceof NavigationStart: {
          if (!this.navigationDialogTimeout) {
            this.navigationDialogTimeout = setTimeout(() => this.dialog.openLoader(), 500);
          }
          break;
        }
        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          clearTimeout(this.navigationDialogTimeout);
          this.navigationDialogTimeout = null;
          this.dialog.closeLoader();
          break;
        }
      }
    });
  }

  private initRouteScrollTop() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      document.documentElement.scrollTop = 0;
    });
  }

  private getInjectedData() {
    this.currentUser.id = this._window.userId || null;
    if (this._window.userToken) {
      this.auth.setAuth(new AuthData({jwtToken: new JwtToken(this._window.userToken)}));
    }
    if (this._window.contextualMenu) {
      this.currentUser.contextualMenu = this._window.contextualMenu;
    }
    if (this._window.applicationsMenu) {
      this.currentUser.applicationsMenu = this._window.applicationsMenu;
    }
    if (this._window.urlPlay) {
      this.currentUser.urlPlay = this._window.urlPlay;
    }
    this.currentUser.refreshUserData();
  }

}

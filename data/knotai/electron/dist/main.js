"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var path = require("path");
require('electron-reload')(path.join(__dirname, "../../dist/knotai/index.html"));
var win;
electron_1.app.on('ready', createWindow);
electron_1.app.on('activate', function () {
    if (win === null) {
        createWindow();
    }
});
function createWindow() {
    win = new electron_1.BrowserWindow({ show: false, frame: true });
    win.once('ready-to-show', function () {
        win.show();
    });
    // win.setMenu(null);
    // win.removeMenu();
    win.setMenuBarVisibility(false);
    /*win.loadURL(
      url.format({
        pathname: path.join(__dirname, `../../dist/knotai/index.html`),
        protocol: 'file:',
        slashes: true,
      })
    );*/
    win.loadURL('http://localhost:4200/');
    win.webContents.openDevTools();
    win.on('closed', function () {
        win = null;
    });
}
/*function openModal() {
  const { BrowserWindow } = require('electron');
  const modal = new BrowserWindow({ parent: win, modal: true, show: false });
  modal.loadURL('https://www.sitepoint.com');
  modal.once('ready-to-show', () => {
    modal.show();
  });
}

ipcMain.on('openModal', (event, arg) => {
  openModal();
});*/
//# sourceMappingURL=main.js.map
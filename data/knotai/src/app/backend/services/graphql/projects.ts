import gql from 'graphql-tag';
import { Injectable, NgIterable, QueryList } from '@angular/core';
import { Uuid } from '../../../shared/types/uuid';
import { Mutation, Query } from 'apollo-angular';

export interface Item {
  id: Uuid;
  name?: string;
  description?: string;
  createdAt?: any;
  updatedAt?: any;
  start?: any;
  finishEstimate?: any;
  finishReal?: any;
  state?: any;
  sprintsByProject?: any;
  priority?: any;
  repository?: any;
  wiki?: string;
}

export interface Pagination {
  projects?: Item[];
  total?: number;
  pagination: {};
  edges: {};
}

export type Wrapper = NgIterable<Pagination> | QueryList<Pagination>;

const GetProjectsDocument = gql`
    query getProjects {
        projects {
            totalCount
            nodes {
                id
            }
            edges {
                node {
                    id
                }
            }
            pageInfo {
                startCursor
                endCursor
            }
        }
    }
`;

const GetProjectsAllInfoDocument = gql`
    query getProjectsAllInfo {
        projects {
            totalCount
            nodes {
                id
                name
                description
                createdAt
                updatedAt
                start
                finishEstimate
                finishReal
                state
                sprintsByProject {
                    totalCount
                    nodes {
                        id
                    }
                }
                priority
                repository
                wiki
            }
            edges {
                node {
                    id
                }
            }
            pageInfo {
                startCursor
                endCursor
            }
        }
    }
`;

const GetProjectDocument = gql`
    query getProject($id: UUID!) {
        project(id: $id) {
            id
            name
            description
            createdAt
            updatedAt
            start
            finishEstimate
            finishReal
            state
            sprintsByProject {
                totalCount
                nodes {
                    id
                }
            }
            priority
            repository
            wiki
        }
    }
`;

const CreateProjectDocument = gql`
    mutation createProject(
        $name: String!,
        $desc: String,
        $start: Datetime,
        $finishEstimate: Datetime,
        $finishReal: Datetime,
        $state: States,
        $priority: Priorities,
        $repo: UUID,
        $wiki: String,
    ) {
        createProject(input: {project: {
            name: $name,
            description: $desc,
            start: $start,
            finishEstimate: $finishEstimate,
            finishReal: $finishReal,
            state: $state,
            priority: $priority,
            repository: $repo,
            wiki: $wiki
        }}) {
            project {
                id
            }
        }
    }
`;

const UpdateProjectDocument = gql`
    mutation updateProject(
        $name: String,
        $desc: String,
        $start: Datetime,
        $finishEstimate: Datetime,
        $finishReal: Datetime,
        $state: States,
        $priority: Priorities,
        $repo: UUID,
        $wiki: String,
    ) {
        updateProject(input: {id: "", patch: {
            name: $name,
            description: $desc,
            start: $start,
            finishEstimate: $finishEstimate,
            finishReal: $finishReal,
            state: $state,
            priority: $priority,
            repository: $repo,
            wiki: $wiki
        }}) {
            project {
                id
            }
        }
    }
`;

const DeleteProjectDocument = gql`
    mutation deleteProject($id: UUID!) {
        deleteProject(input: {id: $id}) {
            deletedProjectNodeId
        }
    }
`;


@Injectable({
  providedIn: 'root'
})
export class GetMany extends Query<any, {}> {
  document = GetProjectsDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetManyAllInfo extends Query<any, {}> {
  document = GetProjectsAllInfoDocument;
}

@Injectable({
  providedIn: 'root'
})
export class GetOne extends Query<any, { id: Uuid }> {
  document = GetProjectDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Create extends Mutation<any, { name: string }> {
  document = CreateProjectDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Update extends Mutation<any, { id: Uuid }> {
  document = UpdateProjectDocument;
}

@Injectable({
  providedIn: 'root'
})
export class Delete extends Mutation<any, { id: Uuid }> {
  document = DeleteProjectDocument;
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministerProjectsComponent } from './administer-projects/administer-projects.component';


const routes: Routes = [
  {
    path: 'administration',
    children: [
      {
        path: '',
        component: AdministerProjectsComponent
      },
      {
        path: 'projects',
        component: AdministerProjectsComponent
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }

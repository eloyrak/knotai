/* tslint:disable:no-unused-expression */
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ProjectAPI } from '../../backend/services/graphql';
import { STATES } from '../../shared/storage/constants';
import { Uuid } from '../../shared/types/uuid';
import { KnotaiProjectModalComponent } from '../../shared/components/knotai-project-modal/knotai-project-modal.component';
import { MatDialog } from '@angular/material';
import { KnotaiConfirmationDialogComponent } from '../../shared/components/knotai-confirmation-dialog/knotai-confirmation-dialog.component';


export interface Action {
  icon: string;
  label: string;
  action: (id: Uuid) => any;
}

const DEFAULT_TABLE_COLUMNS = ['name', 'state', 'actions'];

@Component({
  selector: 'app-administer-projects',
  templateUrl: './administer-projects.component.html',
  styleUrls: ['./administer-projects.component.scss'],
})
export class AdministerProjectsComponent implements OnInit {

  public displayedColumns: string[] = DEFAULT_TABLE_COLUMNS;
  public actions: Action[] = [
    {
      icon: 'create',
      label: 'Edit',
      action: (id: Uuid) => {this.edit(id); },
    },
    {
      icon: 'clear',
      label: 'Delete',
      action: (id: Uuid) => {this.confirmDelete(id); },
    }
  ];
  public projectsGQL: Observable<ProjectAPI.Wrapper>;
  public dataSource: MatTableDataSource<ProjectAPI.Item>;
  public states = STATES;
  public loading = true;

  @ViewChild(MatPaginator, { static: false }) set pagination(paginator: MatPaginator) {
    this.dataSource && (this.dataSource.paginator = paginator);
  }
  @ViewChild(MatSort, { static: false }) set sorting(sort: MatSort) {
    this.dataSource && (this.dataSource.sort = sort);
  }

  constructor(
    private getProjects: ProjectAPI.GetManyAllInfo,
    private getProject: ProjectAPI.GetOne,
    private deleteProject: ProjectAPI.Delete,
    public editionDialog: MatDialog,
    public deleteDialog: MatDialog
  ) {}

  ngOnInit() {
    this.projectsGQL = this.getProjects.watch()
      .valueChanges
      .pipe(
        map(result => {
          return result.data.projects.nodes;
        })
      );
    this.projectsGQL.subscribe(projectsWrapper => {
        const projects = projectsWrapper as unknown as ProjectAPI.Item[];
        this.dataSource = new MatTableDataSource(projects);
        this.loading = false;
      }
      , err => console.error(err), () => {
        this.loading = false;
      });
  }

  public filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public edit(id: Uuid) {
    this.getProject.watch({id})
      .valueChanges
      .pipe(
        map(result => {
          return result.data.project;
        })
      )
      .subscribe(project => {
        const detailDialog = this.editionDialog.open(KnotaiProjectModalComponent, {
          height: 'auto',
          width: 'auto',
          maxWidth: '80%',
          panelClass: 'project-modal',
          data: {
            project
          }
        });
      });
  }

  public confirmDelete(id: Uuid) {
    this.deleteDialog.open(KnotaiConfirmationDialogComponent, {
      height: 'auto',
      width: 'auto',
      maxWidth: '80%',
      panelClass: 'project-delete-confirmation-dialog',
      data: {
        text: 'Are you sure you want to delete this project?'
      }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.delete(id);
        }
      });
  }

  private delete(id: Uuid) {
    this.deleteProject.mutate({id})
      .toPromise()
      .then(result => {
        console.log(result);
      })
      .catch(reason => {
        console.error(reason);
      });
  }
}

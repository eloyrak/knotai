import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrationRoutingModule } from './administration-routing.module';
import { AdministerProjectsComponent } from './administer-projects/administer-projects.component';
import {
  MatFormFieldModule, MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';


@NgModule({
  declarations: [AdministerProjectsComponent],
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule
  ],
  exports: [AdministerProjectsComponent]
})
export class AdministrationModule { }

import { Component, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/typings/sidenav';


export interface SidenavParams {
  mode ?: MatSidenav['mode'];
  opened ?: MatSidenav['opened'];
  topGap ?: MatSidenav['fixedTopGap'];
}

export interface SidenavOption {
  icon: string;
  name: string;
}

const SIDENAV_DEFAULT_PARAMS: SidenavParams = {
  mode: 'side',
  opened: true,
  topGap: 70
};

const SIDENAV_DEFAULT_OPTIONS: SidenavOption[] = [
  {
    icon: 'folder',
    name: 'Projects',
  }
];

@Component({
  selector: 'app-knotai-sidenav',
  templateUrl: './knotai-sidenav.component.html',
  styleUrls: ['./knotai-sidenav.component.scss']
})
export class KnotaiSidenavComponent implements OnInit {

  public sidenavMode: SidenavParams['mode'] = SIDENAV_DEFAULT_PARAMS.mode;
  public sidenavState: SidenavParams['opened'] = SIDENAV_DEFAULT_PARAMS.opened;
  public sidenavTopGap: SidenavParams['topGap'] = SIDENAV_DEFAULT_PARAMS.topGap;

  public sidenavOptions: SidenavOption[] = SIDENAV_DEFAULT_OPTIONS;

  constructor() { }

  ngOnInit() {
  }

}

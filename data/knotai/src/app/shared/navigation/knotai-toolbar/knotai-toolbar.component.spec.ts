import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnotaiToolbarComponent } from './knotai-toolbar.component';

describe('KnotaiToolbarComponent', () => {
  let component: KnotaiToolbarComponent;
  let fixture: ComponentFixture<KnotaiToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnotaiToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnotaiToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

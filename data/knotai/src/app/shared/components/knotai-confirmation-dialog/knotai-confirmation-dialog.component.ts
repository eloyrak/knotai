import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';


const DEFAULT_CONFIRMATION_TEXTS = {
  confirm: 'Continue',
  reject: `I'll think about it`
};

@Component({
  selector: 'app-knotai-confirmation-dialog',
  templateUrl: './knotai-confirmation-dialog.component.html',
  styleUrls: ['./knotai-confirmation-dialog.component.scss']
})
export class KnotaiConfirmationDialogComponent implements OnInit {

  public text: string;
  public dialogTexts = DEFAULT_CONFIRMATION_TEXTS;

  constructor(
    @Inject(MAT_DIALOG_DATA) public entryData: {text: string}
  ) { }

  ngOnInit() {
    this.text = this.entryData.text;
  }

}

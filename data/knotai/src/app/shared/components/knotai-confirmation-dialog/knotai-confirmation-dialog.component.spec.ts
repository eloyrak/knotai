import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnotaiConfirmationDialogComponent } from './knotai-confirmation-dialog.component';

describe('KnotaiConfirmationDialogComponent', () => {
  let component: KnotaiConfirmationDialogComponent;
  let fixture: ComponentFixture<KnotaiConfirmationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnotaiConfirmationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnotaiConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

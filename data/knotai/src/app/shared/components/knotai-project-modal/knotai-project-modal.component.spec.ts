import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnotaiProjectDetailsComponent } from './knotai-project-modal.component';

describe('KnotaiProjectDetailsComponent', () => {
  let component: KnotaiProjectDetailsComponent;
  let fixture: ComponentFixture<KnotaiProjectDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnotaiProjectDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnotaiProjectDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ProjectAPI } from '../../../backend/services/graphql';


@Component({
  selector: 'app-knotai-project-modal',
  templateUrl: './knotai-project-modal.component.html',
  styleUrls: ['./knotai-project-modal.component.scss']
})
export class KnotaiProjectModalComponent implements OnInit {

  public project: ProjectAPI.Item;

  constructor(
    public dialogRef: MatDialogRef<KnotaiProjectModalComponent>,
    @Inject(MAT_DIALOG_DATA) public entryData: {project: ProjectAPI.Item}
  ) { }

  ngOnInit() {
    this.project = this.entryData.project;
  }

  public close(): void {
    this.dialogRef.close();
  }

}

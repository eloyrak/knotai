export const STATES = {
  NEW: 'New',
  PENDING: 'Pending',
  ONGOING: 'Ongoing',
  BLOCKED: 'Blocked',
  REVISION: 'Revision',
  FINISHED: 'Finished'
};

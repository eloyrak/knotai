import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KnotaiSidenavComponent } from './navigation/knotai-sidenav/knotai-sidenav.component';
import { KnotaiToolbarComponent } from './navigation/knotai-toolbar/knotai-toolbar.component';
import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSidenavModule,
  MatToolbarModule
} from '@angular/material';
import { KnotaiProjectCardComponent } from './components/knotai-project-card/knotai-project-card.component';
import { RouterModule } from '@angular/router';
import { KnotaiProjectModalComponent } from './components/knotai-project-modal/knotai-project-modal.component';
import { KnotaiConfirmationDialogComponent } from './components/knotai-confirmation-dialog/knotai-confirmation-dialog.component';



@NgModule({
  declarations: [KnotaiSidenavComponent, KnotaiToolbarComponent, KnotaiProjectCardComponent, KnotaiProjectModalComponent, KnotaiConfirmationDialogComponent],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    RouterModule,
    MatFormFieldModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule
  ],
  exports: [KnotaiSidenavComponent, KnotaiToolbarComponent, KnotaiProjectCardComponent, KnotaiProjectModalComponent, KnotaiConfirmationDialogComponent],
  entryComponents: [KnotaiProjectModalComponent, KnotaiConfirmationDialogComponent]
})
export class SharedModule { }
